# Electric Eyes

This is Electric Eyes... or rather, a GTK4 port of the original Electric
Eyes application by Carsten "Rasterman" Haitzler.

Electric Eyes was one of the first GNOME applications ever written.

Electric Eyes was an impressive image viewing application for Linux in
its heyday. In fact, it was so beloved by some that even after its
maintenance fell by the wayside, it continued to be included in
distributions well after its abandonment; for instance, it was part of
the Mandriva repositories until 2007.

Note that this reboot is in very early stages. It will be considered
feature-complete when it can approximate the functionality of the final
version of Electric Eyes, v. 0.3.12, released in 2000.

Although it is hoped that this reboot will be a competent image viewer,
it is not meant to be a replacement for more modern image viewers for
GNOME such as Loupe.

It is hoped that this project will assist in 2 goals: (1) to keep the
legacy of Electric Eyes alive, and (2) to provide as a decent simple
example of how to create an application with C and GTK4.

The original Electric Eyes application was authored by Carsten
"Rasterman" Haitzler, Copyright © 1998-2000.

This reboot is Copyright © Logan Rathbone, 2024.
