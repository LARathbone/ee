// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#pragma once

#include <gtk/gtk.h>

#include "ee-image.h"
#include "ee-list-data.h"

// TEST
#include "ee-application.h"

G_BEGIN_DECLS

/* Type declaration */

#define EE_TYPE_LIST ee_list_get_type()
G_DECLARE_FINAL_TYPE (EeList, ee_list, EE, LIST, GtkWindow)

/* Method declarations */

GtkWidget *	ee_list_new (void);
GListModel * ee_list_get_model (EeList *self);
GtkSelectionModel * ee_list_get_selection (EeList *self);
gboolean ee_list_get_gen_thumbnails (EeList *self);
gboolean ee_list_get_use_large_thumbnails (EeList *self);
void ee_list_add_entry (EeList *self, EeImage *image);
void ee_list_remove_entry (EeList *list, EeListData *data);

G_END_DECLS
