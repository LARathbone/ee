// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#include "ee-graph.h"

#include <math.h>

/* PROPERTIES */

enum
{
	PROP_0,
	PROP_BRIGHTNESS,
	PROP_CONTRAST,
	PROP_GAMMA,
	PROP_COLOR,
	N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

/* GLOBALS FOR SIGNALS */

enum signal_types {
	SIGNAL_ONE,
	LAST_SIGNAL
};

static guint signals[LAST_SIGNAL];

/* GOBJECT DEFINITION */

struct _EeGraph
{
	GtkDrawingArea parent_instance;

	int brightness;
	int contrast;
	int gamma;
	GdkRGBA color;
};

G_DEFINE_TYPE (EeGraph, ee_graph, GTK_TYPE_DRAWING_AREA)

/* PROPERTIES - GETTERS AND SETTERS */

int
ee_graph_get_brightness (EeGraph *self)
{
	return self->brightness;
}

void
ee_graph_set_brightness (EeGraph *self, int brightness)
{
	self->brightness = brightness;

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_BRIGHTNESS]);

	gtk_widget_queue_draw (GTK_WIDGET(self));
}

static int
ee_graph_get_brightness_factor (EeGraph *self)
{
	int brightness = ee_graph_get_brightness (self);
	int height = gtk_widget_get_height (GTK_WIDGET(self));

	return (brightness / 255.0) * height;
}

int
ee_graph_get_contrast (EeGraph *self)
{
	return self->contrast;
}

void
ee_graph_set_contrast (EeGraph *self, int contrast)
{
	self->contrast = contrast;

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONTRAST]);

	gtk_widget_queue_draw (GTK_WIDGET(self));
}

int
ee_graph_get_gamma (EeGraph *self)
{
	return self->gamma;
}

void
ee_graph_set_gamma (EeGraph *self, int gamma)
{
	self->gamma = gamma;

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_GAMMA]);

	gtk_widget_queue_draw (GTK_WIDGET(self));
}

static double
ee_graph_get_gamma_factor (EeGraph *self)
{
	return (self->gamma / 255.0) * 0.5;
}

void
ee_graph_get_color (EeGraph *self, GdkRGBA *color)
{
	*color = self->color;
}

void
ee_graph_set_color (EeGraph *self, const GdkRGBA *color)
{
	self->color = *color;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_COLOR]);
}

static void
ee_graph_set_property (GObject *object,
		guint property_id,
		const GValue *value,
		GParamSpec *pspec)
{
	EeGraph *self = EE_GRAPH(object);

	switch (property_id)
	{
		case PROP_BRIGHTNESS:
			ee_graph_set_brightness (self, g_value_get_int (value));
			break;

		case PROP_CONTRAST:
			ee_graph_set_contrast (self, g_value_get_int (value));
			break;

		case PROP_GAMMA:
			ee_graph_set_gamma (self, g_value_get_int (value));
			break;

		case PROP_COLOR:
			ee_graph_set_color (self, g_value_get_boxed (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
ee_graph_get_property (GObject *object,
		guint property_id,
		GValue *value,
		GParamSpec *pspec)
{
	EeGraph *self = EE_GRAPH(object);

	switch (property_id)
	{
		case PROP_BRIGHTNESS:
			g_value_set_int (value, ee_graph_get_brightness (self));
			break;

		case PROP_CONTRAST:
			g_value_set_int (value, ee_graph_get_contrast (self));
			break;

		case PROP_GAMMA:
			g_value_set_int (value, ee_graph_get_gamma (self));
			break;

		case PROP_COLOR:
		{
			GdkRGBA tmp = {0};
			ee_graph_get_color (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/* METHOD DEFINITIONS */

inline static void
draw_grid (EeGraph *self, cairo_t *cr, int width, int height)
{
	const int square_size = 8;

	cairo_save (cr);

    cairo_set_line_width (cr, 1);
    cairo_set_source_rgb (cr, 0.5, 0.5, 0.5);

    /* Draw vertical lines */

    for (int x = 0; x <= width; x += square_size) {
        cairo_move_to (cr, x, 0);
        cairo_line_to (cr, x, height);
        cairo_stroke (cr);
    }

    /* Draw horizontal lines */

    for (int y = 0; y <= height; y += square_size) {
        cairo_move_to (cr, 0, y);
        cairo_line_to (cr, width, y);
        cairo_stroke (cr);
    }

	cairo_restore (cr);
}

static void
draw_func (EeGraph *self, cairo_t *cr, int width, int height)
{
	int brightness_factor = ee_graph_get_brightness_factor (self);
	int contrast = ee_graph_get_contrast (self);
	int gamma = ee_graph_get_gamma (self);
	double gamma_factor = ee_graph_get_gamma_factor (self);
	int x1, y1, x2, y2, x3, y3;
	cairo_pattern_t *gradient;

	draw_grid (self, cr, width, height);

	x1 = 0;
	y1 = height;
	x2 = width / 2;
	y2 = height / 2;
	x3 = width;
	y3 = 0;

	if (contrast)
	{
		if (contrast > 0)
		{
			int contrast_factor = (contrast / 255.0) * ((x3-x1) * 0.4);

			x1 += contrast_factor;
			x3 -= contrast_factor;
			x2 = (x1 + x3) / 2;
			y2 = height / 2;
		}
		else
		{
			int contrast_factor = (contrast / 255.0) * ((y1-y3) / 2);

			y1 += contrast_factor;
			y3 -= contrast_factor;
			y2 = (y1 + y3) / 2;
			x2 = width / 2;
		}
	}

	/* Apply gamma */
	if (gamma)
	{
		x2 += gamma_factor * height;
		y2 += gamma_factor * height;
	}

	/* Apply brightness */
	{
		if (x1 > 0)
		{
			x1 -= brightness_factor;
			x2 -= brightness_factor;
			x3 -= brightness_factor;
		}
		else
		{
			y1 -= brightness_factor;
			y2 -= brightness_factor;
			y3 -= brightness_factor;
		}
	}

	/* Start painting */

	cairo_save (cr);

	cairo_move_to (cr, x1, y1);
	cairo_curve_to (cr, x1, y1, x2, y2, x3, y3);

	/* Carve out fill region */

	cairo_line_to (cr, width, 0);
	cairo_line_to (cr, width, height);
	cairo_line_to (cr, 0, height);
	cairo_line_to (cr, x1, y1);
	cairo_close_path (cr);

	/* Draw gradient based on color property */

	gradient = cairo_pattern_create_linear (0.0, 0.0, height, height);
	cairo_pattern_add_color_stop_rgba (gradient, 0.0, self->color.red + 0.8, self->color.green + 0.8, self->color.blue + 0.8, 0.7);
	cairo_pattern_add_color_stop_rgba (gradient, 1.0, self->color.red, self->color.green, self->color.blue, 0.7);
	cairo_set_source (cr, gradient);

	cairo_fill (cr);

	/* Draw line */

	cairo_move_to (cr, x1, y1);
	cairo_curve_to (cr, x1, y1, x2, y2, x3, y3);

	cairo_set_source_rgb (cr, 0, 0, 0);
	cairo_stroke (cr);

	cairo_restore (cr);
}

static void
ee_graph_init (EeGraph *self)
{
	gtk_drawing_area_set_draw_func (GTK_DRAWING_AREA(self), (GtkDrawingAreaDrawFunc)draw_func, NULL, NULL);
}

static void
ee_graph_dispose (GObject *object)
{
	EeGraph *self = EE_GRAPH(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_graph_parent_class)->dispose (object);
}

static void
ee_graph_finalize (GObject *object)
{
	EeGraph *self = EE_GRAPH(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_graph_parent_class)->finalize (object);
}

static void
ee_graph_class_init (EeGraphClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GParamFlags flags = G_PARAM_READWRITE | G_PARAM_CONSTRUCT | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY;

	object_class->dispose =  ee_graph_dispose;
	object_class->finalize = ee_graph_finalize;

	object_class->set_property = ee_graph_set_property;
	object_class->get_property = ee_graph_get_property;

	/* PROPERTIES */

	properties[PROP_BRIGHTNESS] = g_param_spec_int ("brightness", NULL, NULL,
			-255, 255, 0,
			flags);

	properties[PROP_CONTRAST] = g_param_spec_int ("contrast", NULL, NULL,
			-255, 255, 0,
			flags);

	properties[PROP_GAMMA] = g_param_spec_int ("gamma", NULL, NULL,
			-255, 255, 0,
			flags);

	properties[PROP_COLOR] = g_param_spec_boxed ("color", NULL, NULL,
			GDK_TYPE_RGBA,
			flags);

	g_object_class_install_properties (object_class, N_PROPERTIES, properties);

	/* SIGNALS */

	signals[SIGNAL_ONE] = g_signal_new_class_handler ("signal-one",
			G_OBJECT_CLASS_TYPE (object_class),
			G_SIGNAL_RUN_LAST,
		/* no default C function */
			NULL,
		/* defaults for accumulator, marshaller &c. */
			NULL, NULL, NULL,	
		/* No return type or params. */
			G_TYPE_NONE, 0);

}

/* PUBLIC METHOD DEFINITIONS */

GtkWidget *
ee_graph_new (void)
{
	return g_object_new (EE_TYPE_GRAPH, NULL);
}
