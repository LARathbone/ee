// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#include "ee-util.h"

#include <glib/gi18n.h>

/* PUBLIC METHOD DEFINITIONS */

/* Dialogs */

/* GtkDialog is deprecated, but its replacement isn't available till gtk 4.10, so.... */
G_GNUC_BEGIN_IGNORE_DEPRECATIONS

static void
error_dialog_response_cb (GtkDialog *dialog, int response_id, gpointer user_data)
{
	gtk_window_destroy (GTK_WINDOW(dialog));
}

void
ee_error_dialog (GtkWindow *parent, const GError *error)
{
	GtkWidget *dialog;

	if (!parent)
		parent = GTK_WINDOW(ee_application_get_application_window (EE_APPLICATION(g_application_get_default ())));

	dialog = gtk_message_dialog_new (parent,
			GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_ERROR,
			GTK_BUTTONS_CLOSE,
			_("An error has occurred:\n\n%s"),
			error->message);

	g_signal_connect (dialog, "response", G_CALLBACK(error_dialog_response_cb), NULL);
	gtk_application_add_window (GTK_APPLICATION(g_application_get_default ()), GTK_WINDOW(dialog));

	gtk_window_present (GTK_WINDOW(dialog));
}

void
ee_accept_or_reject_dialog (GtkWindow *parent, const char *blurb, const char *accept_label, const char *reject_label, EeUtilAcceptRejectFunc callback, gpointer callback_data)
{
	GtkWidget *dialog;

	if (!parent)
		parent = GTK_WINDOW(ee_application_get_application_window (EE_APPLICATION(g_application_get_default ())));

	dialog = gtk_message_dialog_new (parent,
			GTK_DIALOG_MODAL | GTK_DIALOG_DESTROY_WITH_PARENT,
			GTK_MESSAGE_QUESTION,
			GTK_BUTTONS_NONE,
			"%s",
			blurb);

	gtk_dialog_add_buttons (GTK_DIALOG(dialog),
			reject_label, GTK_RESPONSE_REJECT,
			accept_label, GTK_RESPONSE_ACCEPT,
			NULL);

	g_signal_connect (dialog, "response", G_CALLBACK(callback), callback_data);
	gtk_application_add_window (GTK_APPLICATION(g_application_get_default ()), GTK_WINDOW(dialog));

	gtk_window_present (GTK_WINDOW(dialog));
}

G_GNUC_END_IGNORE_DEPRECATIONS

/* Just a convenience function. */

EeApplicationWindow *
ee_get_application_window (void)
{
	EeApplicationWindow *appwin = ee_application_get_application_window (EE_APPLICATION(g_application_get_default ()));

	return appwin;
}

void
ee_get_screen_size (int *width, int *height)
{
	GdkDisplay *display = gdk_display_get_default ();
	GdkSurface *surface = gtk_native_get_surface (GTK_NATIVE(ee_get_application_window ()));
	GdkMonitor *monitor = gdk_display_get_monitor_at_surface (display, surface);
	GdkRectangle rect = {0};

	gdk_monitor_get_geometry (monitor, &rect);

	*width = rect.width;
	*height = rect.height;
}

/* All-purpose GObject tranform_to function that converts the non-NULL value of
 * an object to 'TRUE'.
 */
gboolean
ee_have_object_transform_to (GBinding *binding,
		const GValue *from_value /* source = GObject */,
		GValue *to_value /* target = gboolean */,
		gpointer data) /* ignored */
{
	GObject *from = g_value_get_object (from_value);
	g_value_set_boolean (to_value, from ? TRUE : FALSE);
	return TRUE;
}
