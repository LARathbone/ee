#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

/* Type declaration */

#define EE_TYPE_GRAPH ee_graph_get_type()
G_DECLARE_FINAL_TYPE (EeGraph, ee_graph, EE, GRAPH, GtkDrawingArea)

/* Method declarations */

GtkWidget * ee_graph_new (void);

G_END_DECLS
