// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#pragma once

#include <gtk/gtk.h>

#include "ee-application.h"

G_BEGIN_DECLS

/* Types */

typedef void (*EeUtilAcceptRejectFunc) (GtkWindow *dialog, int response_id, gpointer user_data);

/* Function declarations */

void ee_error_dialog (GtkWindow *parent, const GError *error);
void ee_accept_or_reject_dialog (GtkWindow *parent, const char *blurb, const char *accept_label, const char *reject_label, EeUtilAcceptRejectFunc callback, gpointer callback_data);
EeApplicationWindow * ee_get_application_window (void);
void ee_get_screen_size (int *width, int *height);

gboolean ee_have_object_transform_to (GBinding *binding, const GValue *from_value /* source = GObject */, GValue *to_value /* target = gboolean */, gpointer data /* ignored */);

G_END_DECLS
