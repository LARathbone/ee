#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

/* Type declaration */

#define EE_TYPE_LIST_TOOLBAR ee_list_toolbar_get_type()
G_DECLARE_FINAL_TYPE (EeListToolbar, ee_list_toolbar, EE, LIST_TOOLBAR, GtkWidget)

/* Method declarations */

GtkWidget *	ee_list_toolbar_new (void);

G_END_DECLS
