#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

/* Type declaration */

#define EE_TYPE_SESSION ee_session_get_type()
G_DECLARE_FINAL_TYPE (EeSession, ee_session, EE, SESSION, GObject)

/* Method declarations */

EeSession *	ee_session_new (void);

G_END_DECLS
