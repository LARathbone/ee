// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#pragma once

#include <gtk/gtk.h>

#include "ee-toolbar.h"
#include "ee-image.h"

G_BEGIN_DECLS

/* Type declaration */

#define EE_TYPE_APPLICATION_WINDOW ee_application_window_get_type()
G_DECLARE_FINAL_TYPE (EeApplicationWindow, ee_application_window, EE, APPLICATION_WINDOW, GtkApplicationWindow)

/* Method declarations */

GtkWidget *	ee_application_window_new (void);
EeImage * ee_application_window_get_image (EeApplicationWindow *self);
void ee_application_window_set_image (EeApplicationWindow *self, EeImage *image);
void ee_application_window_set_display_texture_type (EeApplicationWindow *self, EeImageTextureType type);
void ee_application_window_refresh_image (EeApplicationWindow *self);
int ee_application_window_get_image_display_width (EeApplicationWindow *self);
int ee_application_window_get_image_display_height (EeApplicationWindow *self);
gboolean ee_application_window_get_scrolled_view (EeApplicationWindow *self);
void ee_application_window_set_scrolled_view (EeApplicationWindow *self, gboolean scrolled);
gboolean ee_application_window_get_show_toolbar (EeApplicationWindow *self);
GtkPositionType ee_application_window_get_toolbar_pos (EeApplicationWindow *self);

G_END_DECLS
