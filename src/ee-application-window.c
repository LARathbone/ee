// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#include "ee-application-window.h"
#include "ee-util.h"

#include <glib/gi18n.h>
#include "ee-enums.h"

/* PROPERTIES */

enum
{
	PROP_TOOLBAR_POS = 1,
	PROP_SHOW_TOOLBAR,
	PROP_SCROLLED_VIEW,
	PROP_IMAGE,
	PROP_DISPLAY_TEXTURE_TYPE,
	PROP_CROP_DIMENSIONS,
	N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

/* GOBJECT DEFINITION */

struct _EeApplicationWindow
{
	GtkApplicationWindow parent_instance;

	EeImage *image;
	GtkGesture *gesture_click;
	GtkWidget *main_context_menu;
	GtkPositionType toolbar_pos;
	EeImageTextureType display_texture_type;

	/* From template */
	GtkWidget *grid;
	GtkWidget *toolbar;
	GtkWidget *picture_box;
	GtkWidget *scrolled_window;
	GtkWidget *scrolled_picture;
	GtkWidget *fixed_picture;
};

G_DEFINE_TYPE (EeApplicationWindow, ee_application_window, GTK_TYPE_APPLICATION_WINDOW)

/* PROPERTIES - GETTERS AND SETTERS */

void
ee_application_window_set_toolbar_pos (EeApplicationWindow *self, GtkPositionType pos)
{
	GtkLayoutChild *toolbar_layout_child = gtk_layout_manager_get_layout_child (gtk_widget_get_layout_manager (self->grid), self->toolbar);
	GtkLayoutChild *picture_layout_child = gtk_layout_manager_get_layout_child (gtk_widget_get_layout_manager (self->grid), self->picture_box);

	if (pos == GTK_POS_TOP || pos == GTK_POS_BOTTOM)
		gtk_orientable_set_orientation (GTK_ORIENTABLE(self->toolbar), GTK_ORIENTATION_HORIZONTAL);
	else
		gtk_orientable_set_orientation (GTK_ORIENTABLE(self->toolbar), GTK_ORIENTATION_VERTICAL);

	switch (pos)
	{
		case GTK_POS_TOP:
			g_object_set (toolbar_layout_child, "column", 0, "row", 0, NULL);
			g_object_set (picture_layout_child, "column", 0, "row", 1, NULL);
			break;

		case GTK_POS_BOTTOM:
			g_object_set (toolbar_layout_child, "column", 0, "row", 1, NULL);
			g_object_set (picture_layout_child, "column", 0, "row", 0, NULL);
			break;

		case GTK_POS_LEFT:
			g_object_set (toolbar_layout_child, "column", 0, "row", 0, NULL);
			g_object_set (picture_layout_child, "column", 1, "row", 0, NULL);
			break;

		case GTK_POS_RIGHT:
			g_object_set (toolbar_layout_child, "column", 1, "row", 0, NULL);
			g_object_set (picture_layout_child, "column", 0, "row", 0, NULL);
			break;

		default:
			g_assert_not_reached ();
			break;
	}

	self->toolbar_pos = pos;

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_TOOLBAR_POS]);
}

GtkPositionType
ee_application_window_get_toolbar_pos (EeApplicationWindow *self)
{
	return self->toolbar_pos;
}

gboolean
ee_application_window_get_show_toolbar (EeApplicationWindow *self)
{
	return gtk_widget_get_visible (GTK_WIDGET(self->toolbar));
}

void
ee_application_window_set_show_toolbar (EeApplicationWindow *self, gboolean show)
{
	gtk_widget_set_visible (GTK_WIDGET(self->toolbar), show);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_SHOW_TOOLBAR]);
}

gboolean
ee_application_window_get_scrolled_view (EeApplicationWindow *self)
{
	return gtk_widget_get_visible (self->scrolled_window);
}

void
ee_application_window_set_scrolled_view (EeApplicationWindow *self, gboolean scrolled)
{
	if (scrolled)
	{
		gtk_widget_set_visible (self->scrolled_window, TRUE);
		gtk_widget_set_visible (self->fixed_picture, FALSE);

		/* Since default-width and default-height are connected to notifying of
		 * the crop-dimensions property, emit these notify signals here to
		 * ensure the crop size is properly refreshed when toggling
		 * scrolled-view on.
		 */
		g_object_notify (G_OBJECT(self), "default-width");
		g_object_notify (G_OBJECT(self), "default-height");
	}
	else
	{
		gtk_widget_set_visible (self->scrolled_window, FALSE);
		gtk_widget_set_visible (self->fixed_picture, TRUE);
	}

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_SCROLLED_VIEW]);
}

EeImageTextureType
ee_application_window_get_display_texture_type (EeApplicationWindow *self)
{
	return self->display_texture_type;
}

void
ee_application_window_set_display_texture_type (EeApplicationWindow *self, EeImageTextureType type)
{
	GdkTexture *texture;

	switch (type)
	{
		case EE_IMAGE_TEXTURE_TYPE_CLEAN:
			texture = ee_image_get_clean_texture (self->image);
			break;

		case EE_IMAGE_TEXTURE_TYPE_DIRTY:
			texture = ee_image_get_dirty_texture (self->image);
			break;

		default:
			g_assert_not_reached ();
	}

	gtk_picture_set_paintable (GTK_PICTURE(self->fixed_picture), GDK_PAINTABLE(texture));

	gtk_picture_set_paintable (GTK_PICTURE(self->scrolled_picture), GDK_PAINTABLE(texture));

	self->display_texture_type = type;

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_DISPLAY_TEXTURE_TYPE]); 
}

/* transfer none */
EeImage *
ee_application_window_get_image (EeApplicationWindow *self)
{
	return self->image;
}

/* Set the default display texture after image is set so as to avoid race
 * conditions - the default texture type must be established before we set the
 * GtkPicture textures to their default types.
 */
inline static void
setup_default_textures (EeApplicationWindow *self)
{
	const GValue *default_display_texture_value = g_param_spec_get_default_value (properties[PROP_DISPLAY_TEXTURE_TYPE]);
	EeImageTextureType type = g_value_get_enum (default_display_texture_value);

	ee_application_window_set_display_texture_type (self, type);
}

inline static void
show_splash (EeApplicationWindow *self)
{
	g_autoptr(GdkTexture) tex = gdk_texture_new_from_resource ("/com/gitlab/LARathbone/EE/images/ee-splash.xpm");

	gtk_picture_set_paintable (GTK_PICTURE(self->fixed_picture), GDK_PAINTABLE(tex));
	gtk_picture_set_paintable (GTK_PICTURE(self->scrolled_picture), GDK_PAINTABLE(tex));

	g_clear_object (&self->image);

	/* cross-reference: ui file. The closure doesn't seem to work when the
	 * property is null. I have no idea why.
	 */
	gtk_window_set_title (GTK_WINDOW(self), _("Electric Eyes"));
}

/* transfer none */
void
ee_application_window_set_image (EeApplicationWindow *self, EeImage *image)
{
	int column, row, width, height;

	if (!image) {
		show_splash (self);
		goto out;
	}

	g_clear_object (&self->image);
	self->image = g_object_ref (image);

	setup_default_textures (self);

out:
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_IMAGE]);
}

void
ee_application_window_get_crop_dimensions (EeApplicationWindow *self, GdkRectangle *rect)
{
	GdkRectangle tmp = {0};
	gboolean retval = ee_image_get_crop_dimensions (self->image, self->scrolled_picture, self->picture_box, &tmp);

	if (!retval)
		g_critical ("%s: Unable to get crop dimensions", __func__);

	*rect = tmp;
}

static void
ee_application_window_set_property (GObject *object,
		guint property_id,
		const GValue *value,
		GParamSpec *pspec)
{
	EeApplicationWindow *self = EE_APPLICATION_WINDOW(object);

	switch (property_id)
	{
		case PROP_IMAGE:
			ee_application_window_set_image (self, g_value_get_object (value));
			break;

		case PROP_TOOLBAR_POS:
			ee_application_window_set_toolbar_pos (self, g_value_get_enum (value));
			break;

		case PROP_SHOW_TOOLBAR:
			ee_application_window_set_show_toolbar (self, g_value_get_boolean (value));
			break;

		case PROP_SCROLLED_VIEW:
			ee_application_window_set_scrolled_view (self, g_value_get_boolean (value));
			break;

		case PROP_DISPLAY_TEXTURE_TYPE:
			ee_application_window_set_display_texture_type (self, g_value_get_enum (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
ee_application_window_get_property (GObject *object,
		guint property_id,
		GValue *value,
		GParamSpec *pspec)
{
	EeApplicationWindow *self = EE_APPLICATION_WINDOW(object);

	switch (property_id)
	{
		case PROP_IMAGE:
			g_value_set_object (value, ee_application_window_get_image (self));
			break;

		case PROP_TOOLBAR_POS:
			g_value_set_enum (value, ee_application_window_get_toolbar_pos (self));
			break;

		case PROP_SHOW_TOOLBAR:
			g_value_set_boolean (value, ee_application_window_get_show_toolbar (self));
			break;

		case PROP_SCROLLED_VIEW:
			g_value_set_boolean (value, ee_application_window_get_scrolled_view (self));
			break;

		case PROP_DISPLAY_TEXTURE_TYPE:
			g_value_set_enum (value, ee_application_window_get_display_texture_type (self));
			break;

		case PROP_CROP_DIMENSIONS:
		{
			GdkRectangle tmp;
			ee_application_window_get_crop_dimensions (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/* PRIVATE METHOD DEFINITIONS */

char *
get_title_closure (EeApplicationWindow *self, EeImage *image, gboolean modified)
{
	/* This would be nice, wouldn't it? Unfortunately it doesn't work. See the
	 * _set_image function and the .ui file for more comments.
	 */
	if (!image)
		return g_strdup (_("Electric Eyes"));
	else
	{
		g_autofree char *filename = g_file_get_basename (ee_image_get_file (image));

		if (modified)
			return g_strdup_printf ("* %s", filename); 
		else
			return g_strdup (filename);
	}
}

static void
main_context_menu_cb (EeApplicationWindow *self,
		gint n_press,
		gdouble x,
		gdouble y,
		GtkGestureClick *gesture)
{
	gtk_popover_set_pointing_to (GTK_POPOVER (self->main_context_menu), &(const GdkRectangle){.x=x, .y=y});
	gtk_popover_popup (GTK_POPOVER(self->main_context_menu));
}

inline static void
setup_main_context_menu (EeApplicationWindow *self)
{
	g_autoptr(GtkBuilder) builder = gtk_builder_new_from_resource ("/com/gitlab/LARathbone/EE/ee-main-menu.ui");

	self->main_context_menu = (GtkWidget *) gtk_builder_get_object (builder, "ee_main_context_menu");
	gtk_widget_set_parent (GTK_WIDGET(self->main_context_menu), GTK_WIDGET(self->picture_box));

	self->gesture_click = gtk_gesture_click_new ();

	/* right-click only */
	gtk_gesture_single_set_button (GTK_GESTURE_SINGLE(self->gesture_click), GDK_BUTTON_SECONDARY);

	g_signal_connect_swapped (self->gesture_click, "pressed", G_CALLBACK(main_context_menu_cb), self);

	gtk_widget_add_controller (GTK_WIDGET(self->picture_box), GTK_EVENT_CONTROLLER(self->gesture_click));
}

static void
setup_actions (GtkWidgetClass *widget_class)
{
	gtk_widget_class_install_property_action (widget_class, "win.show-toolbar", "show-toolbar");
	gtk_widget_class_install_property_action (widget_class, "win.toolbar-position", "toolbar-position");
	gtk_widget_class_install_property_action (widget_class, "win.scrolled-view", "scrolled-view");

	gtk_widget_class_add_binding_action (widget_class, GDK_KEY_t, GDK_CONTROL_MASK, "win.show-toolbar", NULL);
	gtk_widget_class_add_binding_action (widget_class, GDK_KEY_s, GDK_CONTROL_MASK, "win.scrolled-view", NULL);
}

static void
crop_action (EeApplicationWindow *self)
{
	ee_image_crop (self->image, self->scrolled_picture, self->picture_box);
	ee_application_window_refresh_image (self);
}

gboolean
crop_transform_to (GBinding *binding,
		const GValue *from_value /* source = gboolean */,
		GValue *to_value /* target = gboolean */,
		EeApplicationWindow *self)
{
	gboolean scrolled_view = g_value_get_boolean (from_value);
	g_value_set_boolean (to_value, self->image && scrolled_view ? TRUE : FALSE);
	return TRUE;
}

/* Special case re: action because it needs to be done in _init as opposed to
 * class_init in order to use bindings.
 */
inline static void
setup_crop_action (EeApplicationWindow *self)
{
	GSimpleAction *action = g_simple_action_new ("crop", NULL);

	g_action_map_add_action (G_ACTION_MAP(self), G_ACTION(action));
	g_signal_connect_swapped (action, "activate", G_CALLBACK(crop_action), self);
	g_object_bind_property_full (self, "scrolled-view", action, "enabled", G_BINDING_SYNC_CREATE, (GBindingTransformFunc) crop_transform_to, NULL, self, NULL);
}

static void
crop_dimensions_cb (GObject *self)
{
	g_object_notify_by_pspec (self, properties[PROP_CROP_DIMENSIONS]);
}

inline static void
setup_crop_size_signals (EeApplicationWindow *self)
{
	g_signal_connect (self, "notify::default-width", G_CALLBACK(crop_dimensions_cb), NULL);
	g_signal_connect (self, "notify::default-height", G_CALLBACK(crop_dimensions_cb), NULL);

	g_signal_connect_swapped (gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW(self->scrolled_window)), "notify::value", G_CALLBACK(crop_dimensions_cb), self);
	g_signal_connect_swapped (gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW(self->scrolled_window)), "notify::value", G_CALLBACK(crop_dimensions_cb), self);
}

static void
ee_application_window_init (EeApplicationWindow *self)
{
	gtk_widget_init_template (GTK_WIDGET(self));

	setup_main_context_menu (self);
	setup_crop_action (self);
	setup_crop_size_signals (self);
}

static void
ee_application_window_dispose (GObject *object)
{
	EeApplicationWindow *self = EE_APPLICATION_WINDOW(object);

	g_clear_object (&self->image);
	g_clear_pointer (&self->main_context_menu, gtk_widget_unparent);

	/* Chain up */
	G_OBJECT_CLASS(ee_application_window_parent_class)->dispose (object);
}

static void
ee_application_window_finalize (GObject *object)
{
	EeApplicationWindow *self = EE_APPLICATION_WINDOW(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_application_window_parent_class)->finalize (object);
}

static gboolean
ee_application_window_close_request (GtkWindow *window)
{
	EeApplicationWindow *self = EE_APPLICATION_WINDOW(window);
	EeApplication *app = EE_APPLICATION(gtk_window_get_application (window));

	ee_application_quit (app);

	return TRUE;
}

static void
ee_application_window_class_init (EeApplicationWindowClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);
	GtkWindowClass *window_class = GTK_WINDOW_CLASS(klass);
	GParamFlags flags = G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY;

	object_class->dispose =  ee_application_window_dispose;
	object_class->finalize = ee_application_window_finalize;
	object_class->set_property = ee_application_window_set_property;
	object_class->get_property = ee_application_window_get_property;

	window_class->close_request = ee_application_window_close_request;

	/* PROPERTIES */

	properties[PROP_TOOLBAR_POS] = g_param_spec_enum ("toolbar-position", NULL, NULL,
			GTK_TYPE_POSITION_TYPE,
			GTK_POS_TOP,
			flags | G_PARAM_CONSTRUCT);

	properties[PROP_SHOW_TOOLBAR] = g_param_spec_boolean ("show-toolbar", NULL, NULL,
			FALSE,
			flags | G_PARAM_CONSTRUCT);

	properties[PROP_SCROLLED_VIEW] = g_param_spec_boolean ("scrolled-view", NULL, NULL,
			FALSE,
			flags | G_PARAM_CONSTRUCT);

	properties[PROP_IMAGE] = g_param_spec_object ("image", NULL, NULL,
			EE_TYPE_IMAGE,
			flags | G_PARAM_CONSTRUCT);

	properties[PROP_DISPLAY_TEXTURE_TYPE] = g_param_spec_enum ("display-texture", NULL, NULL,
			EE_TYPE_IMAGE_TEXTURE_TYPE,
			EE_IMAGE_TEXTURE_TYPE_CLEAN,
			flags);

	properties[PROP_CROP_DIMENSIONS] = g_param_spec_boxed ("crop-dimensions", NULL, NULL,
			GDK_TYPE_RECTANGLE,
			G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties (object_class, N_PROPERTIES, properties);

	/* ACTIONS */
	setup_actions (widget_class);

	/* TEMPLATE */

	g_type_ensure (EE_TYPE_TOOLBAR);

	gtk_widget_class_set_template_from_resource (widget_class, "/com/gitlab/LARathbone/EE/ee-application-window.ui");

	gtk_widget_class_bind_template_child (widget_class, EeApplicationWindow, grid);
	gtk_widget_class_bind_template_child (widget_class, EeApplicationWindow, toolbar);
	gtk_widget_class_bind_template_child (widget_class, EeApplicationWindow, picture_box);
	gtk_widget_class_bind_template_child (widget_class, EeApplicationWindow, scrolled_window);
	gtk_widget_class_bind_template_child (widget_class, EeApplicationWindow, scrolled_picture);
	gtk_widget_class_bind_template_child (widget_class, EeApplicationWindow, fixed_picture);

	gtk_widget_class_bind_template_callback (widget_class, get_title_closure);
}

/* PUBLIC METHOD DEFINITIONS */

GtkWidget *
ee_application_window_new (void)
{
	return g_object_new (EE_TYPE_APPLICATION_WINDOW, NULL);
}

/* Convenience function to refresh the image from its texture regardless whether dirty or clean. Mostly useful for scaling operations since they apply to both textures for the time being.
 */
void
ee_application_window_refresh_image (EeApplicationWindow *self)
{
	ee_application_window_set_display_texture_type (self, self->display_texture_type);
}

int
ee_application_window_get_image_display_width (EeApplicationWindow *self)
{
	g_return_val_if_fail (gtk_widget_get_visible (self->fixed_picture), 0);

	return gtk_widget_get_width (self->fixed_picture);
}

int
ee_application_window_get_image_display_height (EeApplicationWindow *self)
{
	g_return_val_if_fail (gtk_widget_get_visible (self->fixed_picture), 0);

	return gtk_widget_get_height (self->fixed_picture);
}
