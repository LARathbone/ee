#include "ee-list-toolbar.h"

/* PROPERTIES */

enum
{
	PROP_ONE = 1,
	N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

/* GOBJECT DEFINITION */

struct _EeListToolbar
{
	GtkWidget parent_instance;
};

G_DEFINE_TYPE (EeListToolbar, ee_list_toolbar, GTK_TYPE_WIDGET)

/* PROPERTIES - GETTERS AND SETTERS */

static void
ee_list_toolbar_set_property (GObject *object,
		guint property_id,
		const GValue *value,
		GParamSpec *pspec)
{
	EeListToolbar *self = EE_LIST_TOOLBAR(object);

	switch (property_id)
	{
		case PROP_ONE:
			/* --- */
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
ee_list_toolbar_get_property (GObject *object,
		guint property_id,
		GValue *value,
		GParamSpec *pspec)
{
	EeListToolbar *self = EE_LIST_TOOLBAR(object);

	switch (property_id)
	{
		case PROP_ONE:
			/* --- */
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/* METHOD DEFINITIONS */

static void
ee_list_toolbar_init (EeListToolbar *self)
{
	gtk_widget_init_template (GTK_WIDGET(self));
}

static void
ee_list_toolbar_dispose (GObject *object)
{
	EeListToolbar *self = EE_LIST_TOOLBAR(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_list_toolbar_parent_class)->dispose (object);
}

static void
ee_list_toolbar_finalize (GObject *object)
{
	EeListToolbar *self = EE_LIST_TOOLBAR(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_list_toolbar_parent_class)->finalize (object);
}

static void
ee_list_toolbar_class_init (EeListToolbarClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

	object_class->dispose =  ee_list_toolbar_dispose;
	object_class->finalize = ee_list_toolbar_finalize;
	object_class->set_property = ee_list_toolbar_set_property;
	object_class->get_property = ee_list_toolbar_get_property;

	/* PROPERTIES */

	properties[PROP_ONE] = g_param_spec_string ("property-one",
			"Property one",
			"Our lovely first property",
			/* default: */	"Hello, world!",
			G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

	g_object_class_install_properties (object_class, N_PROPERTIES, properties);

	/* TEMPLATE */

	gtk_widget_class_set_template_from_resource (widget_class, "/com/gitlab/LARathbone/EE/ee-list-toolbar.ui");
}

/* PUBLIC METHOD DEFINITIONS */

GtkWidget *
ee_list_toolbar_new (void)
{
	return g_object_new (EE_TYPE_LIST_TOOLBAR, NULL);
}
