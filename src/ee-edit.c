// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#include "ee-edit.h"

#include "ee-enums.h"
#include <glib/gi18n.h>

/* PROPERTIES */

enum
{
	PROP_IMAGE = 1,
	PROP_GAMMA_RGBA_MOD,
	PROP_BRIGHTNESS_RGBA_MOD,
	PROP_CONTRAST_RGBA_MOD,
	PROP_GRAY_GAMMA_RGBA_MOD,
	PROP_GRAY_BRIGHTNESS_RGBA_MOD,
	PROP_GRAY_CONTRAST_RGBA_MOD,
	PROP_CONSOLIDATED_GAMMA_RGBA_MOD,
	PROP_CONSOLIDATED_BRIGHTNESS_RGBA_MOD,
	PROP_CONSOLIDATED_CONTRAST_RGBA_MOD,
	PROP_ALWAYS_APPLY,
	N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

/* GOBJECT DEFINITION */

struct _EeEdit
{
	GtkWindow parent_instance;

	EeImage *image;

	EeRGBAMod gamma_mod;
	EeRGBAMod brightness_mod;
	EeRGBAMod contrast_mod;

	EeRGBAMod gray_gamma_mod;
	EeRGBAMod gray_brightness_mod;
	EeRGBAMod gray_contrast_mod;

	EeRGBAMod consolidated_gamma_mod;
	EeRGBAMod consolidated_brightness_mod;
	EeRGBAMod consolidated_contrast_mod;

	/* Template */
	GtkWidget *mini;
	GtkWidget *gamma_slider;
	GtkWidget *brightness_slider;
	GtkWidget *contrast_slider;
	GtkAdjustment *gamma_adj;
	GtkAdjustment *brightness_adj;
	GtkAdjustment *contrast_adj;
	GtkAdjustment *gray_gamma_adj;
	GtkAdjustment *gray_brightness_adj;
	GtkAdjustment *gray_contrast_adj;
	GtkWidget *gray_toggle_button;
	GtkWidget *red_toggle_button;
	GtkWidget *green_toggle_button;
	GtkWidget *blue_toggle_button;
	GtkWidget *controls_label;
	GtkWidget *apply_button;
	GtkWidget *keep_button;
	GtkWidget *always_apply_checkbtn;
	GtkWidget *custom_size_button;
	GtkAdjustment *custom_height_adj;
	GtkAdjustment *custom_width_adj;
	GtkWidget *current_view_size_apply_button;
	GtkWidget *crop_size_label;
	GtkWidget *gray_graph;
	GtkWidget *red_graph;
	GtkWidget *green_graph;
	GtkWidget *blue_graph;
};

G_DEFINE_TYPE (EeEdit, ee_edit, GTK_TYPE_WINDOW)

/* FORWARD DECLARATIONS */

static void reset_action (EeEdit *self);
static void apply_clicked_cb (EeEdit *self);
static void keep_clicked_cb (EeEdit *self);

/* PROPERTIES - GETTERS AND SETTERS */

#define CREATE_TRANSFORM_TO(X) \
	EeRGBAMod rgba_mod = self->X ## _mod; \
	int adj_val = g_value_get_double (from_value); \
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->red_toggle_button))) \
	{ \
		rgba_mod.red_mod = adj_val; \
		g_object_set (self->red_graph, #X, adj_val, NULL); \
	} \
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->green_toggle_button))) \
	{ \
		rgba_mod.green_mod = adj_val; \
		g_object_set (self->green_graph, #X, adj_val, NULL); \
	} \
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->blue_toggle_button))) \
	{ \
		rgba_mod.blue_mod = adj_val; \
		g_object_set (self->blue_graph, #X, adj_val, NULL); \
	} \
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->gray_toggle_button))) \
	{ \
		rgba_mod = self->gray_ ## X ## _mod; \
		rgba_mod.red_mod = adj_val; \
		rgba_mod.green_mod = adj_val; \
		rgba_mod.blue_mod = adj_val; \
		g_object_set (self->gray_graph, #X, adj_val, NULL); \
	} \
	else \
	{ \
		return FALSE; \
	} \
	g_value_set_boxed (to_value, &rgba_mod); \
	return TRUE;

static gboolean
brightness_transform_to (GBinding *binding, const GValue *from_value, GValue *to_value, EeEdit *self)
{
	CREATE_TRANSFORM_TO (brightness)
}

static gboolean
contrast_transform_to (GBinding *binding, const GValue *from_value, GValue *to_value, EeEdit *self)
{
	CREATE_TRANSFORM_TO (contrast)
}

static gboolean
gamma_transform_to (GBinding *binding, const GValue *from_value, GValue *to_value, EeEdit *self)
{
	CREATE_TRANSFORM_TO (gamma)
}

static gboolean
rgba_mod_transform_from (GBinding *binding, const GValue *from_value, GValue *to_value, EeEdit *self)
{
	EeRGBAMod *rgba_mod;
	double adj_val;

	rgba_mod = g_value_get_boxed (from_value);

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->red_toggle_button)))
	{
		adj_val = rgba_mod->red_mod;
	}
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->green_toggle_button)))
	{
		adj_val = rgba_mod->green_mod;
	}
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->blue_toggle_button)))
	{
		adj_val = rgba_mod->blue_mod;
	}
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->gray_toggle_button)))
	{
		/* just use any of them as they're sync'd anyway */
		adj_val = rgba_mod->red_mod;
	}
	else
		return FALSE;

	g_value_set_double (to_value, adj_val);
	return TRUE;
}

static void
setup_image_bindings (EeEdit *self)
{
	static GBinding *binding1;
	static GBinding *binding1a;
	static GBinding *binding1b;

	static GBinding *binding2;
	static GBinding *binding2a;
	static GBinding *binding2b;

	static GBinding *binding3;
	static GBinding *binding3a;
	static GBinding *binding3b;

	g_object_bind_property (self->image, "dirty-texture", self->mini, "paintable", G_BINDING_SYNC_CREATE);

	reset_action (self);

	g_clear_object (&binding1);
	g_clear_object (&binding1a);
	g_clear_object (&binding1b);

	g_clear_object (&binding2);
	g_clear_object (&binding2a);
	g_clear_object (&binding2b);

	g_clear_object (&binding3);
	g_clear_object (&binding3a);
	g_clear_object (&binding3b);

	binding1 = g_object_bind_property_full (self->brightness_adj, "value", self, "brightness-rgba-mod", G_BINDING_BIDIRECTIONAL, (GBindingTransformFunc)brightness_transform_to, (GBindingTransformFunc)rgba_mod_transform_from, self, NULL);

	binding1a = g_object_bind_property_full (self->gray_brightness_adj, "value", self, "gray-brightness-rgba-mod", G_BINDING_BIDIRECTIONAL, (GBindingTransformFunc)brightness_transform_to, (GBindingTransformFunc)rgba_mod_transform_from, self, NULL);

	binding1b = g_object_bind_property (self, "consolidated-brightness-rgba-mod", self->image, "brightness-rgba-mod", G_BINDING_DEFAULT);

	binding2 = g_object_bind_property_full (self->contrast_adj, "value", self, "contrast-rgba-mod", G_BINDING_BIDIRECTIONAL, (GBindingTransformFunc)contrast_transform_to, (GBindingTransformFunc)rgba_mod_transform_from, self, NULL);

	binding2a = g_object_bind_property_full (self->gray_contrast_adj, "value", self, "gray-contrast-rgba-mod", G_BINDING_BIDIRECTIONAL, (GBindingTransformFunc)contrast_transform_to, (GBindingTransformFunc)rgba_mod_transform_from, self, NULL);

	binding2b = g_object_bind_property (self, "consolidated-contrast-rgba-mod", self->image, "contrast-rgba-mod", G_BINDING_DEFAULT);

	binding3 = g_object_bind_property_full (self->gamma_adj, "value", self, "gamma-rgba-mod", G_BINDING_BIDIRECTIONAL, (GBindingTransformFunc)gamma_transform_to, (GBindingTransformFunc)rgba_mod_transform_from, self, NULL);

	binding3a = g_object_bind_property_full (self->gray_gamma_adj, "value", self, "gray-gamma-rgba-mod", G_BINDING_BIDIRECTIONAL, (GBindingTransformFunc)gamma_transform_to, (GBindingTransformFunc)rgba_mod_transform_from, self, NULL);

	binding3b = g_object_bind_property (self, "consolidated-gamma-rgba-mod", self->image, "gamma-rgba-mod", G_BINDING_DEFAULT);
}

static void
always_apply_checkbtn_notify_active_cb (EeEdit *self)
{
	EeApplicationWindow *appwin = ee_get_application_window ();
	static gulong sig1, sig2, sig3;
	gboolean checked = gtk_check_button_get_active (GTK_CHECK_BUTTON(self->always_apply_checkbtn));

	if (checked)
	{
		if (!sig1) {
			sig1 = g_signal_connect (self, "notify::consolidated-brightness-rgba-mod", G_CALLBACK(apply_clicked_cb), NULL);
			sig2 = g_signal_connect (self, "notify::consolidated-contrast-rgba-mod", G_CALLBACK(apply_clicked_cb), NULL);
			sig3 = g_signal_connect (self, "notify::consolidated-gamma-rgba-mod", G_CALLBACK(apply_clicked_cb), NULL);
		}
	}
	else
	{
		g_clear_signal_handler (&sig1, self);
		g_clear_signal_handler (&sig2, self);
		g_clear_signal_handler (&sig3, self);
	}
}

static void
setup_always_apply_signal (EeEdit *self)
{
	g_signal_connect_swapped (self->always_apply_checkbtn, "notify::active", G_CALLBACK(always_apply_checkbtn_notify_active_cb), self);
}

/* transfer none */
void
ee_edit_set_image (EeEdit *self, EeImage *image)
{
	if (! image)
		goto notify_and_out;

	self->image = g_object_ref (image);

	setup_image_bindings (self);
	setup_always_apply_signal (self);

notify_and_out:
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_IMAGE]);
}

inline static void
ee_edit_set_brightness_rgba_mod (EeEdit *self, const EeRGBAMod *mod)
{
	self->brightness_mod = *mod;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_BRIGHTNESS_RGBA_MOD]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONSOLIDATED_BRIGHTNESS_RGBA_MOD]);
}

inline static void
ee_edit_get_brightness_rgba_mod (EeEdit *self, EeRGBAMod *out)
{
	*out = self->brightness_mod;
}

inline static void
ee_edit_set_contrast_rgba_mod (EeEdit *self, const EeRGBAMod *mod)
{
	self->contrast_mod = *mod;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONTRAST_RGBA_MOD]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONSOLIDATED_CONTRAST_RGBA_MOD]);
}

inline static void
ee_edit_get_contrast_rgba_mod (EeEdit *self, EeRGBAMod *out)
{
	*out = self->contrast_mod;
}

inline static void
ee_edit_set_gamma_rgba_mod (EeEdit *self, const EeRGBAMod *mod)
{
	self->gamma_mod = *mod;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_GAMMA_RGBA_MOD]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONSOLIDATED_GAMMA_RGBA_MOD]);
}

inline static void
ee_edit_get_gamma_rgba_mod (EeEdit *self, EeRGBAMod *out)
{
	*out = self->gamma_mod;
}

inline static void
ee_edit_set_gray_contrast_rgba_mod (EeEdit *self, const EeRGBAMod *mod)
{
	self->gray_contrast_mod = *mod;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_GRAY_CONTRAST_RGBA_MOD]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONSOLIDATED_CONTRAST_RGBA_MOD]);
}

inline static void
ee_edit_get_gray_contrast_rgba_mod (EeEdit *self, EeRGBAMod *out)
{
	*out = self->gray_contrast_mod;
}

inline static void
ee_edit_set_gray_brightness_rgba_mod (EeEdit *self, const EeRGBAMod *mod)
{
	self->gray_brightness_mod = *mod;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_GRAY_BRIGHTNESS_RGBA_MOD]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONSOLIDATED_BRIGHTNESS_RGBA_MOD]);
}

inline static void
ee_edit_get_gray_brightness_rgba_mod (EeEdit *self, EeRGBAMod *out)
{
	*out = self->gray_brightness_mod;
}

inline static void
ee_edit_set_gray_gamma_rgba_mod (EeEdit *self, const EeRGBAMod *mod)
{
	self->gray_gamma_mod = *mod;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_GRAY_GAMMA_RGBA_MOD]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONSOLIDATED_GAMMA_RGBA_MOD]);
}

inline static void
ee_edit_get_gray_gamma_rgba_mod (EeEdit *self, EeRGBAMod *out)
{
	*out = self->gray_gamma_mod;
}

/* get_consolidated_*: read only */
inline static void
ee_edit_get_consolidated_contrast_rgba_mod (EeEdit *self, EeRGBAMod *out)
{
	ee_rgba_mod_add (&self->contrast_mod, &self->gray_contrast_mod, &self->consolidated_contrast_mod);
	*out = self->consolidated_contrast_mod;
}

inline static void
ee_edit_get_consolidated_brightness_rgba_mod (EeEdit *self, EeRGBAMod *out)
{
	ee_rgba_mod_add (&self->brightness_mod, &self->gray_brightness_mod, &self->consolidated_brightness_mod);
	*out = self->consolidated_brightness_mod;
}

inline static void
ee_edit_get_consolidated_gamma_rgba_mod (EeEdit *self, EeRGBAMod *out)
{
	ee_rgba_mod_add (&self->gamma_mod, &self->gray_gamma_mod, &self->consolidated_gamma_mod);
	*out = self->consolidated_gamma_mod;
}

gboolean
ee_edit_get_always_apply (EeEdit *self)
{
	return gtk_check_button_get_active (GTK_CHECK_BUTTON(self->always_apply_checkbtn));
}

void
ee_edit_set_always_apply (EeEdit *self, gboolean always)
{
	gtk_check_button_set_active (GTK_CHECK_BUTTON(self->always_apply_checkbtn), always);
}

static void
ee_edit_set_property (GObject *object,
		guint property_id,
		const GValue *value,
		GParamSpec *pspec)
{
	EeEdit *self = EE_EDIT(object);

	switch (property_id)
	{
		case PROP_IMAGE:
			ee_edit_set_image (self, g_value_get_object (value));
			break;

		case PROP_BRIGHTNESS_RGBA_MOD:
			ee_edit_set_brightness_rgba_mod (self, g_value_get_boxed (value));
			break;

		case PROP_CONTRAST_RGBA_MOD:
			ee_edit_set_contrast_rgba_mod (self, g_value_get_boxed (value));
			break;

		case PROP_GAMMA_RGBA_MOD:
			ee_edit_set_gamma_rgba_mod (self, g_value_get_boxed (value));
			break;

		case PROP_GRAY_BRIGHTNESS_RGBA_MOD:
			ee_edit_set_gray_brightness_rgba_mod (self, g_value_get_boxed (value));
			break;

		case PROP_GRAY_CONTRAST_RGBA_MOD:
			ee_edit_set_gray_contrast_rgba_mod (self, g_value_get_boxed (value));
			break;

		case PROP_GRAY_GAMMA_RGBA_MOD:
			ee_edit_set_gray_gamma_rgba_mod (self, g_value_get_boxed (value));
			break;

		case PROP_ALWAYS_APPLY:
			ee_edit_set_always_apply (self, g_value_get_boolean (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
ee_edit_get_property (GObject *object,
		guint property_id,
		GValue *value,
		GParamSpec *pspec)
{
	EeEdit *self = EE_EDIT(object);

	switch (property_id)
	{
		case PROP_IMAGE:
			g_value_set_object (value, self->image);
			break;

		case PROP_BRIGHTNESS_RGBA_MOD:
		{
			EeRGBAMod tmp;
			ee_edit_get_brightness_rgba_mod (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		case PROP_CONTRAST_RGBA_MOD:
		{
			EeRGBAMod tmp;
			ee_edit_get_contrast_rgba_mod (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		case PROP_GAMMA_RGBA_MOD:
		{
			EeRGBAMod tmp;
			ee_edit_get_gamma_rgba_mod (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		case PROP_GRAY_BRIGHTNESS_RGBA_MOD:
		{
			EeRGBAMod tmp;
			ee_edit_get_gray_brightness_rgba_mod (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		case PROP_GRAY_CONTRAST_RGBA_MOD:
		{
			EeRGBAMod tmp;
			ee_edit_get_gray_contrast_rgba_mod (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		case PROP_GRAY_GAMMA_RGBA_MOD:
		{
			EeRGBAMod tmp;
			ee_edit_get_gray_gamma_rgba_mod (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		case PROP_CONSOLIDATED_BRIGHTNESS_RGBA_MOD:
		{
			EeRGBAMod tmp;
			ee_edit_get_consolidated_brightness_rgba_mod (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		case PROP_CONSOLIDATED_CONTRAST_RGBA_MOD:
		{
			EeRGBAMod tmp;
			ee_edit_get_consolidated_contrast_rgba_mod (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		case PROP_CONSOLIDATED_GAMMA_RGBA_MOD:
		{
			EeRGBAMod tmp;
			ee_edit_get_consolidated_gamma_rgba_mod (self, &tmp);
			g_value_set_boxed (value, &tmp);
		}
			break;

		case PROP_ALWAYS_APPLY:
			g_value_set_boolean (value, ee_edit_get_always_apply (self));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/* GtkBuilder CLOSURES & SIGNAL CALLBACKS */

static void
apply_clicked_cb (EeEdit *self)
{
	EeApplicationWindow *appwin = ee_get_application_window ();

	/* This essentially causes a refresh as well so no need to call that function here. */
	ee_application_window_set_display_texture_type (appwin, EE_IMAGE_TEXTURE_TYPE_DIRTY);
}

static void
keep_clicked_cb (EeEdit *self)
{
	apply_clicked_cb (self);
	ee_image_apply_changes (self->image);
	reset_action (self);
}


static void
snap_screen_ready_cb (XdpPortal *portal, GAsyncResult *res, EeEdit *self)
{
	g_autoptr(GFile) file = NULL;
	g_autoptr(GError) local_error = NULL;
	g_autofree char *uri = xdp_portal_take_screenshot_finish (portal, res, &local_error);

	if (local_error) {
		g_critical ("%s: Error: %s", __func__, local_error->message);
		return;
	}

	file = g_file_new_for_uri (uri);
	ee_application_open_file (EE_APPLICATION(g_application_get_default ()), file);
}

static void
snap_window_button_cb (EeEdit *self, GtkButton *btn)
{
	XdpParent *parent = xdp_parent_new_gtk (GTK_WINDOW(ee_get_application_window ()));
	XdpPortal *portal = xdp_portal_new ();

	xdp_portal_take_screenshot (portal, parent, XDP_SCREENSHOT_FLAG_INTERACTIVE, NULL, (GAsyncReadyCallback)snap_screen_ready_cb, self);
}

static void
snap_screen_button_cb (EeEdit *self, GtkButton *btn)
{
	XdpParent *parent = xdp_parent_new_gtk (GTK_WINDOW(ee_get_application_window ()));
	XdpPortal *portal = xdp_portal_new ();

	xdp_portal_take_screenshot (portal, parent, XDP_SCREENSHOT_FLAG_NONE, NULL, (GAsyncReadyCallback)snap_screen_ready_cb, self);
}

static char *
get_image_size_label_closure (EeEdit *self, int width, int height)
{
	return g_strdup_printf (_("Image Size: (%d x %d)"), width, height);
}

static void
custom_size_apply_button_cb (EeEdit *self, GtkButton *btn)
{
	ee_image_set_width (self->image, gtk_adjustment_get_value (self->custom_width_adj));
	ee_image_set_height (self->image, gtk_adjustment_get_value (self->custom_height_adj));
	ee_application_window_refresh_image (ee_get_application_window ());
}

static void
current_view_size_apply_button_cb (EeEdit *self, GtkButton *btn)
{
	EeApplicationWindow *appwin = ee_get_application_window ();

	ee_image_set_width (self->image, ee_application_window_get_image_display_width (appwin));
	ee_image_set_height (self->image, ee_application_window_get_image_display_height (appwin));
	ee_application_window_refresh_image (appwin);
}

static void
max_aspect_button_cb (EeEdit *self, GtkButton *btn)
{
	EeApplicationWindow *appwin = ee_get_application_window ();
	int img_width = ee_image_get_width (self->image);
	int img_height = ee_image_get_height (self->image);
	int screen_width, screen_height;
	float width_ratio, height_ratio, scale_factor;

	ee_get_screen_size (&screen_width, &screen_height);

	if (img_width >= screen_width || img_height >= screen_height)
		return;

	width_ratio = screen_width / img_width;
	height_ratio = screen_height / img_height;
	scale_factor = MIN (width_ratio, height_ratio);

	ee_image_set_width (self->image, img_width * scale_factor);
	ee_image_set_height (self->image, img_height * scale_factor);

	ee_application_window_refresh_image (appwin);
}

static void
max_size_button_cb (EeEdit *self, GtkButton *btn)
{
	EeApplicationWindow *appwin = ee_get_application_window ();
	int screen_width, screen_height;

	ee_get_screen_size (&screen_width, &screen_height);
	ee_image_set_width (self->image, screen_width);
	ee_image_set_height (self->image, screen_height);
	ee_application_window_refresh_image (appwin);

	// FIXME - move this.

	/* NOTE: This only seems to work once on X11. I believe it should work on Wayland.
	 * See: https://gitlab.gnome.org/GNOME/gtk/-/issues/5919
	 */
	gtk_window_set_default_size (GTK_WINDOW(appwin), screen_width, screen_height);
}

static void
rotate_button_cb (EeEdit *self, GtkButton *btn)
{
	GdkPixbufRotation old_rotation = ee_image_get_rotation (self->image);
	GdkPixbufRotation new_rotation = 0;

	switch (old_rotation)
	{
		case 0:
			new_rotation = 90;
			break;
		case 90:
			new_rotation = 180;
			break;
		case 180:
			new_rotation = 270;
			break;
		case 270:
			new_rotation = 0;
			break;
		default:
			g_assert_not_reached ();
			break;
	}
	ee_image_set_rotation (self->image, new_rotation);
	ee_application_window_refresh_image (ee_get_application_window ());
}

/* PRIVATE METHOD DEFINITIONS */

#define CREATE_RESET_ACTION(X) \
	EeEdit *self = EE_EDIT(widget); \
	EeRGBAMod rgba_mod = self-> X ## _mod; \
	if (!self->mini) return; \
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->red_toggle_button))) \
		rgba_mod.red_mod = 0; \
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->green_toggle_button))) \
		rgba_mod.green_mod = 0; \
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->blue_toggle_button))) \
		rgba_mod.blue_mod = 0; \
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->gray_toggle_button))) \
	{ \
		rgba_mod = self->gray_ ## X ## _mod; \
		rgba_mod.red_mod = 0; \
		rgba_mod.green_mod = 0; \
		rgba_mod.blue_mod = 0; \
		ee_edit_set_gray_ ## X ## _rgba_mod (self, &rgba_mod); \
		return; \
	} \
	else \
		/* Sometimes GTK will report that NO buttons are active, so just return and don't use g_assert_not_reached here. This comment applies to all similar switch-style code in this source file. */ \
		return; \
	ee_edit_set_ ## X ## _rgba_mod (self, &rgba_mod);

static void
reset_brightness_action (GtkWidget *widget, const char *action_name, GVariant *parameter)
{
	CREATE_RESET_ACTION (brightness)
}

static void
reset_contrast_action (GtkWidget *widget, const char *action_name, GVariant *parameter)
{
	CREATE_RESET_ACTION (contrast)
}

static void
reset_gamma_action (GtkWidget *widget, const char *action_name, GVariant *parameter)
{
	CREATE_RESET_ACTION (gamma)
}

inline static void
reset_graphs (EeEdit *self)
{
	GtkWidget *graphs[] = {self->gray_graph, self->red_graph, self->green_graph, self->blue_graph, NULL};

	for (int i = 0; graphs[i]; ++i)
		g_object_set (graphs[i], "brightness", 0, "contrast", 0, "gamma", 0, NULL);
}

static void
reset_action (EeEdit *self)
{
	ee_edit_set_brightness_rgba_mod (self, &(EeRGBAMod){0,0,0,0});
	ee_edit_set_gray_brightness_rgba_mod (self, &(EeRGBAMod){0,0,0,0});

	ee_edit_set_contrast_rgba_mod (self, &(EeRGBAMod){0,0,0,0});
	ee_edit_set_gray_contrast_rgba_mod (self, &(EeRGBAMod){0,0,0,0});

	ee_edit_set_gamma_rgba_mod (self, &(EeRGBAMod){0,0,0,0});
	ee_edit_set_gray_gamma_rgba_mod (self, &(EeRGBAMod){0,0,0,0});

	reset_graphs (self);
}

static void
scale_action (EeEdit *self, const char *action_name, GVariant *parameter)
{
	double factor = g_variant_get_double (parameter);

	if (G_APPROX_VALUE (factor, 1.0, 0.001))
	{
		ee_image_restore_original_size (self->image);
	}
	else
	{
		int width = ee_image_get_width (self->image) * factor;
		int height = ee_image_get_height (self->image) * factor;

		ee_image_set_width (self->image, width);
		ee_image_set_height (self->image, height);
	}

	/* Queue appwin to refresh image based on scaled texture */
	ee_application_window_refresh_image (ee_get_application_window ());
}

static void
flip_action (EeEdit *self, const char *action_name, GVariant *parameter)
{
	EeImageFlipType flip = g_variant_get_boolean (parameter);
	
	ee_image_flip (self->image, flip);
	ee_application_window_refresh_image (ee_get_application_window ());
}

static void
toggle_button_update_label_cb (EeEdit *self)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->red_toggle_button)))
		gtk_label_set_label (GTK_LABEL(self->controls_label), _("Red Controls"));
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->green_toggle_button)))
		gtk_label_set_label (GTK_LABEL(self->controls_label), _("Green Controls"));
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->blue_toggle_button)))
		gtk_label_set_label (GTK_LABEL(self->controls_label), _("Blue Controls"));
	else
		gtk_label_set_label (GTK_LABEL(self->controls_label), _("Gray Controls"));
}

static void
toggle_button_notify_value_cb (EeEdit *self)
{
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_BRIGHTNESS_RGBA_MOD]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONTRAST_RGBA_MOD]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_GAMMA_RGBA_MOD]);
}

static void
toggle_button_notify_adjustments_cb (EeEdit *self)
{
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->red_toggle_button)) || gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->green_toggle_button)) || gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->blue_toggle_button)))
	{
		gtk_range_set_adjustment (GTK_RANGE(self->gamma_slider), self->gamma_adj);
		gtk_range_set_adjustment (GTK_RANGE(self->brightness_slider), self->brightness_adj);
		gtk_range_set_adjustment (GTK_RANGE(self->contrast_slider), self->contrast_adj);
		gtk_range_set_adjustment (GTK_RANGE(self->gamma_slider), self->gamma_adj);
	}
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->gray_toggle_button)))
	{
		gtk_range_set_adjustment (GTK_RANGE(self->gamma_slider), self->gray_gamma_adj);
		gtk_range_set_adjustment (GTK_RANGE(self->brightness_slider), self->gray_brightness_adj);
		gtk_range_set_adjustment (GTK_RANGE(self->contrast_slider), self->gray_contrast_adj);
		gtk_range_set_adjustment (GTK_RANGE(self->gamma_slider), self->gray_gamma_adj);
	}
	else
		return;
}

static void
toggle_button_graph_bindings_cb (EeEdit *self)
{
	GtkWidget *graph = NULL;
	GtkAdjustment *brightness_adj = self->brightness_adj;
	GtkAdjustment *contrast_adj = self->contrast_adj;
	GtkAdjustment *gamma_adj = self->gamma_adj;
	static GBinding *brightness_binding;
	static GBinding *contrast_binding;
	static GBinding *gamma_binding;

	g_clear_object (&brightness_binding);
	g_clear_object (&contrast_binding);
	g_clear_object (&gamma_binding);

	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->red_toggle_button)))
		graph = self->red_graph;
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->green_toggle_button)))
		graph = self->green_graph;
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->blue_toggle_button)))
		graph = self->blue_graph;
	else if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON(self->gray_toggle_button)))
	{
		graph = self->gray_graph;
		brightness_adj = self->gray_brightness_adj;
		contrast_adj = self->gray_contrast_adj;
		gamma_adj = self->gray_gamma_adj;
	}
	else
		return;

	brightness_binding = g_object_bind_property (brightness_adj, "value", graph, "brightness", G_BINDING_DEFAULT);
	contrast_binding = g_object_bind_property (contrast_adj, "value", graph, "contrast", G_BINDING_DEFAULT);
	gamma_binding = g_object_bind_property (gamma_adj, "value", graph, "gamma", G_BINDING_DEFAULT);
}

static void
setup_toggle_signals (EeEdit *self)
{
	GtkWidget *buttons[] = {self->gray_toggle_button, self->red_toggle_button, self->green_toggle_button, self->blue_toggle_button, NULL};

	for (int i = 0; buttons[i] != NULL; ++i)
	{
		/* `_ Controls` label */
		g_signal_connect_swapped (buttons[i], "notify::active", G_CALLBACK(toggle_button_update_label_cb), self);

		/* Toggling should notify::value of the adjustment to sync dials and rgba levels */
		g_signal_connect_swapped (buttons[i], "notify::active", G_CALLBACK(toggle_button_notify_value_cb), self);

		/* Setup adjustments for sliders */
		g_signal_connect_swapped (buttons[i], "notify::active", G_CALLBACK(toggle_button_notify_adjustments_cb), self);

		/* Setup bindings for graphs */
		g_signal_connect_swapped (buttons[i], "notify::active", G_CALLBACK(toggle_button_graph_bindings_cb), self);
	}

	for (int i = 0; buttons[i] != NULL; ++i)
		g_object_notify (G_OBJECT(buttons[i]), "active");
}

static gboolean
crop_size_label_transform_to (GBinding *binding,
		const GValue *from_value,
		GValue *to_value,
		EeEdit *self)
{
	g_autofree char *str = NULL;
	const GdkRectangle *rect = g_value_get_boxed (from_value);

	if (!self->image)
		return FALSE;

	/* Translators: the integers below are coordinates/width/height. ie, (x, y) (w x h)
	 */
	str = g_strdup_printf (_("Crop: (%d, %d) (%d x %d)"), rect->x, rect->y, rect->width, rect->height);
	g_value_set_string (to_value, str);

	return TRUE;
}

static void
setup_crop_label_bindings (EeEdit *self, GParamSpec *pspec, EeApplicationWindow *appwin)
{
	static GBinding *binding;

	g_clear_object (&binding);

	if (ee_application_window_get_scrolled_view (appwin))
	{
		binding = g_object_bind_property_full (appwin, "crop-dimensions", self->crop_size_label, "label",
				G_BINDING_DEFAULT,
				(GBindingTransformFunc) crop_size_label_transform_to,
				NULL,
				self,
				NULL);
	}
}

static void
setup_fixed_image_view_only_bindings (EeEdit *self)
{
	EeApplicationWindow *appwin = ee_get_application_window ();

	g_object_bind_property (appwin, "scrolled-view", self->current_view_size_apply_button, "sensitive", G_BINDING_SYNC_CREATE | G_BINDING_INVERT_BOOLEAN);
	g_object_bind_property (appwin, "scrolled-view", self->crop_size_label, "sensitive", G_BINDING_SYNC_CREATE);

	g_signal_connect_object (appwin, "notify::scrolled-view", G_CALLBACK(setup_crop_label_bindings), self, G_CONNECT_SWAPPED);
}

static gboolean
ee_edit_close_request (GtkWindow *window)
{
	GObject *app = G_OBJECT(gtk_window_get_application (window));

	gtk_widget_set_visible (GTK_WIDGET(window), FALSE);

	g_object_notify (app, "show-edit-window");

	return TRUE;
}

static void
ee_edit_init (EeEdit *self)
{
	g_type_ensure (EE_TYPE_GRAPH);

	gtk_widget_init_template (GTK_WIDGET(self));

	setup_toggle_signals (self);
	setup_fixed_image_view_only_bindings (self);
}

static void
ee_edit_dispose (GObject *object)
{
	EeEdit *self = EE_EDIT(object);

	g_clear_object (&self->image);

	/* Chain up */
	G_OBJECT_CLASS(ee_edit_parent_class)->dispose (object);
}

static void
ee_edit_finalize (GObject *object)
{
	EeEdit *self = EE_EDIT(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_edit_parent_class)->finalize (object);
}

static void
ee_edit_class_init (EeEditClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);
	GtkWindowClass *window_class = GTK_WINDOW_CLASS(klass);
	GParamFlags flags = G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY;

	object_class->dispose =  ee_edit_dispose;
	object_class->finalize = ee_edit_finalize;
	object_class->set_property = ee_edit_set_property;
	object_class->get_property = ee_edit_get_property;

	window_class->close_request = ee_edit_close_request;

	/* PROPERTIES */

	properties[PROP_IMAGE] = g_param_spec_object ("image", NULL, NULL,
			EE_TYPE_IMAGE,
			flags);

	properties[PROP_BRIGHTNESS_RGBA_MOD] = g_param_spec_boxed ("brightness-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			flags);

	properties[PROP_CONTRAST_RGBA_MOD] = g_param_spec_boxed ("contrast-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			flags);

	properties[PROP_GAMMA_RGBA_MOD] = g_param_spec_boxed ("gamma-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			flags);

	properties[PROP_GRAY_BRIGHTNESS_RGBA_MOD] = g_param_spec_boxed ("gray-brightness-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			flags);

	properties[PROP_GRAY_CONTRAST_RGBA_MOD] = g_param_spec_boxed ("gray-contrast-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			flags);

	properties[PROP_GRAY_GAMMA_RGBA_MOD] = g_param_spec_boxed ("gray-gamma-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			flags);

	properties[PROP_CONSOLIDATED_BRIGHTNESS_RGBA_MOD] = g_param_spec_boxed ("consolidated-brightness-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

	properties[PROP_CONSOLIDATED_CONTRAST_RGBA_MOD] = g_param_spec_boxed ("consolidated-contrast-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

	properties[PROP_CONSOLIDATED_GAMMA_RGBA_MOD] = g_param_spec_boxed ("consolidated-gamma-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

	properties[PROP_ALWAYS_APPLY] = g_param_spec_boolean ("always-apply", NULL, NULL,
			FALSE,
			flags);

	g_object_class_install_properties (object_class, N_PROPERTIES, properties);

	/* ACTIONS */

	gtk_widget_class_install_action (widget_class, "win.reset-brightness", NULL, reset_brightness_action);
	gtk_widget_class_install_action (widget_class, "win.reset-contrast", NULL, reset_contrast_action);
	gtk_widget_class_install_action (widget_class, "win.reset-gamma", NULL, reset_gamma_action);
	gtk_widget_class_install_action (widget_class, "win.reset", NULL, (GtkWidgetActionActivateFunc)reset_action);
	gtk_widget_class_install_action (widget_class, "win.scale", "d", (GtkWidgetActionActivateFunc)scale_action);
	gtk_widget_class_install_action (widget_class, "win.flip", "b", (GtkWidgetActionActivateFunc)flip_action);

	/* TEMPLATE */

	gtk_widget_class_set_template_from_resource (widget_class, "/com/gitlab/LARathbone/EE/ee-edit.ui");

	gtk_widget_class_bind_template_child (widget_class, EeEdit, mini);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, gamma_slider);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, brightness_slider);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, contrast_slider);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, gamma_adj);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, brightness_adj);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, contrast_adj);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, gray_gamma_adj);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, gray_brightness_adj);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, gray_contrast_adj);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, gray_toggle_button);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, red_toggle_button);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, green_toggle_button);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, blue_toggle_button);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, controls_label);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, apply_button);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, keep_button);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, always_apply_checkbtn);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, custom_size_button);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, custom_width_adj);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, custom_height_adj);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, current_view_size_apply_button);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, crop_size_label);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, gray_graph);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, red_graph);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, green_graph);
	gtk_widget_class_bind_template_child (widget_class, EeEdit, blue_graph);

	gtk_widget_class_bind_template_callback (widget_class, get_image_size_label_closure);
	gtk_widget_class_bind_template_callback (widget_class, custom_size_apply_button_cb);
	gtk_widget_class_bind_template_callback (widget_class, current_view_size_apply_button_cb);
	gtk_widget_class_bind_template_callback (widget_class, max_aspect_button_cb);
	gtk_widget_class_bind_template_callback (widget_class, max_size_button_cb);
	gtk_widget_class_bind_template_callback (widget_class, rotate_button_cb);
	gtk_widget_class_bind_template_callback (widget_class, snap_screen_button_cb);
	gtk_widget_class_bind_template_callback (widget_class, snap_window_button_cb);
	gtk_widget_class_bind_template_callback (widget_class, apply_clicked_cb);
	gtk_widget_class_bind_template_callback (widget_class, keep_clicked_cb);
}

/* PUBLIC METHOD DEFINITIONS */

GtkWidget *
ee_edit_new (void)
{
	return g_object_new (EE_TYPE_EDIT, NULL);
}
