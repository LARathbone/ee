// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#include "ee-image.h"
#include "ee-util.h"

#include <glib/gi18n.h>
#include <string.h>
#include <math.h>

/* Unsophisticated lookup table to try to guess image file type based on extension
 */
static const char *img_extension_lookup_table[] = {
	"jpg",	"jpeg",
	"jpeg",	"jpeg",
	"png",	"png",
	"ico",	"ico",
	"bmp",	"bmp",
	NULL
};

static const char *
img_extension_lookup (const char *key)
{
	const char *value = NULL;

	for (int i = 0; img_extension_lookup_table[i] != NULL; i += 2)
	{
		if (g_strcmp0 (key, img_extension_lookup_table[i]) == 0)
			value = img_extension_lookup_table[i + 1];
	}

	return value;
}

/* transfer full */
inline static char *
get_extension (const char *basename)
{
	char *p;

	p = strrchr (basename, '.');

	if (p && *(p + 1) != '\0')
		return g_strdup (p + 1);

	return NULL;
}

/* EeRGBAMod BOXED TYPE */

G_DEFINE_BOXED_TYPE (EeRGBAMod, ee_rgba_mod, ee_rgba_mod_copy, ee_rgba_mod_free)

EeRGBAMod *
ee_rgba_mod_copy (const EeRGBAMod *rgba_mod)
{
	EeRGBAMod *copy;

	copy = g_new (EeRGBAMod, 1);
	memcpy (copy, rgba_mod, sizeof (EeRGBAMod));

	return copy;
}

void
ee_rgba_mod_free (EeRGBAMod *rgba_mod)
{
	g_free (rgba_mod);
}

void
ee_rgba_mod_add (const EeRGBAMod *mod1, const EeRGBAMod *mod2, EeRGBAMod *out)
{
	EeRGBAMod new = {0};
	int tmp;

	tmp = mod1->red_mod + mod2->red_mod;
	new.red_mod = CLAMP (tmp, -255, 255);

	tmp = mod1->green_mod + mod2->green_mod;
	new.green_mod = CLAMP (tmp, -255, 255);

	tmp = mod1->blue_mod + mod2->blue_mod;
	new.blue_mod = CLAMP (tmp, -255, 255);

	tmp = mod1->alpha_mod + mod2->alpha_mod;
	new.alpha_mod = CLAMP (tmp, -255, 255);

	*out = new;
}

/* PROPERTIES */

enum
{
	PROP_FILE = 1,
	PROP_DIRTY_TEXTURE,
	PROP_CLEAN_TEXTURE,
	PROP_BRIGHTNESS_RGBA_MOD,
	PROP_CONTRAST_RGBA_MOD,
	PROP_GAMMA_RGBA_MOD,
	PROP_MODIFIED,
	PROP_WIDTH,
	PROP_HEIGHT,
	PROP_ROTATION,
	N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

/* GOBJECT DEFINITION */

struct _EeImage
{
	GObject parent_instance;

	GFile *file;
	GdkPixbuf *clean_pixbuf;
	GdkPixbuf *dirty_pixbuf;
	EeRGBAMod brightness_rgba_mod;
	EeRGBAMod gamma_rgba_mod;
	EeRGBAMod contrast_rgba_mod;
	gboolean modified;
#define CACHED_DEFAULT -1
	int cached_height;
	int cached_width;
	GdkPixbufRotation rotation;
};

G_DEFINE_TYPE (EeImage, ee_image, G_TYPE_OBJECT)

/* PRIVATE METHODS */

static void ee_image_set_modified (EeImage *self, gboolean modified);

inline static void
clamp_pixel_color (int *pixel)
{
	*pixel = CLAMP (*pixel, 0, 255);
}

inline static void
apply_brightness_to_pixel (EeImage *self, EeImageChannel channel, const guchar *clean_pixels, int offset, guchar *dirty_pixels)
{
	int mod;
	int new_pixel_color;

	switch (channel) {
		case EE_IMAGE_CHANNEL_RED:
			mod = self->brightness_rgba_mod.red_mod;
			break;
		case EE_IMAGE_CHANNEL_GREEN:
			mod = self->brightness_rgba_mod.green_mod;
			break;
		case EE_IMAGE_CHANNEL_BLUE:
			mod = self->brightness_rgba_mod.blue_mod;
			break;
		case EE_IMAGE_CHANNEL_ALPHA:
			mod = self->brightness_rgba_mod.alpha_mod;
			break;
		default:
			g_assert_not_reached ();
	}	

	new_pixel_color = clean_pixels[channel + offset] + mod;
	clamp_pixel_color (&new_pixel_color);
	dirty_pixels[channel + offset] = new_pixel_color;
}

inline static void
apply_contrast_to_pixel (EeImage *self, EeImageChannel channel, const guchar *clean_pixels, int offset, guchar *dirty_pixels)
{
	/* mostly for human readability - these will likely all get optimized out. */
	int new_pixel_color, old_color, contrast, factor;
	int mod;

	switch (channel) {
		case EE_IMAGE_CHANNEL_RED:
			mod = self->contrast_rgba_mod.red_mod;
			break;
		case EE_IMAGE_CHANNEL_GREEN:
			mod = self->contrast_rgba_mod.green_mod;
			break;
		case EE_IMAGE_CHANNEL_BLUE:
			mod = self->contrast_rgba_mod.blue_mod;
			break;
		case EE_IMAGE_CHANNEL_ALPHA:
			mod = self->contrast_rgba_mod.alpha_mod;
			break;
		default:
			g_assert_not_reached ();
	}	

	old_color = dirty_pixels[channel + offset];
	contrast = mod;
	factor = (259 * (contrast + 255)) / (255 * (259 - contrast));
	new_pixel_color = factor * (old_color - 128) + 128;
	clamp_pixel_color (&new_pixel_color);
	dirty_pixels[channel + offset] = new_pixel_color;
}

inline static void
apply_gamma_to_pixel (EeImage *self, EeImageChannel channel, const guchar *clean_pixels, int offset, guchar *dirty_pixels)
{
	/* mostly for human readability - these will likely all get optimized out. */
	int new_pixel_color, old_color, gamma, factor;
	float gamma_value, value_f;
	int mod;

	switch (channel) {
		case EE_IMAGE_CHANNEL_RED:
			mod = self->gamma_rgba_mod.red_mod;
			break;
		case EE_IMAGE_CHANNEL_GREEN:
			mod = self->gamma_rgba_mod.green_mod;
			break;
		case EE_IMAGE_CHANNEL_BLUE:
			mod = self->gamma_rgba_mod.blue_mod;
			break;
		case EE_IMAGE_CHANNEL_ALPHA:
			mod = self->gamma_rgba_mod.alpha_mod;
			break;
		default:
			g_assert_not_reached ();
	}	

	gamma = mod;
	gamma_value = 1.0f + gamma / 255.0f;
	old_color = clean_pixels[channel + offset];
	value_f = old_color / 255.0f;
	value_f = powf (value_f, gamma_value);
	new_pixel_color = value_f * 255.0f;
	clamp_pixel_color (&new_pixel_color);
	dirty_pixels[channel + offset] = new_pixel_color;
}

static gboolean
refresh_dirty_pixbuf_rgb (gpointer data)
{
	EeImage *self = EE_IMAGE(data);
	int n_channels = gdk_pixbuf_get_n_channels (self->clean_pixbuf);
	guchar *dirty_pixels = NULL;
	guchar *clean_pixels = NULL;
	guint dirty_len, clean_len, len;
	gboolean alpha = FALSE;

	g_return_val_if_fail (n_channels == 3 || n_channels == 4, FALSE);

	if (n_channels == 4)
		alpha = TRUE;

	dirty_pixels = gdk_pixbuf_get_pixels_with_length (self->dirty_pixbuf, &dirty_len);
	clean_pixels = gdk_pixbuf_get_pixels_with_length (self->clean_pixbuf, &clean_len);

	g_assert (dirty_len == clean_len);

	len = dirty_len;

	for (guint i = 0; i < len; i += n_channels)
	{
		apply_brightness_to_pixel (self, EE_IMAGE_CHANNEL_RED, clean_pixels, i, dirty_pixels);
		apply_brightness_to_pixel (self, EE_IMAGE_CHANNEL_GREEN, clean_pixels, i, dirty_pixels);
		apply_brightness_to_pixel (self, EE_IMAGE_CHANNEL_BLUE, clean_pixels, i, dirty_pixels);

		if (alpha) {
			apply_brightness_to_pixel (self, EE_IMAGE_CHANNEL_ALPHA, clean_pixels, i, dirty_pixels);
		}

		apply_contrast_to_pixel (self, EE_IMAGE_CHANNEL_RED, dirty_pixels, i, dirty_pixels);
		apply_contrast_to_pixel (self, EE_IMAGE_CHANNEL_GREEN, dirty_pixels, i, dirty_pixels);
		apply_contrast_to_pixel (self, EE_IMAGE_CHANNEL_BLUE, dirty_pixels, i, dirty_pixels);

		if (alpha) {
			apply_contrast_to_pixel (self, EE_IMAGE_CHANNEL_ALPHA, dirty_pixels, i, dirty_pixels);
		}

		apply_gamma_to_pixel (self, EE_IMAGE_CHANNEL_RED, dirty_pixels, i, dirty_pixels);
		apply_gamma_to_pixel (self, EE_IMAGE_CHANNEL_GREEN, dirty_pixels, i, dirty_pixels);
		apply_gamma_to_pixel (self, EE_IMAGE_CHANNEL_BLUE, dirty_pixels, i, dirty_pixels);

		if (alpha) {
			apply_gamma_to_pixel (self, EE_IMAGE_CHANNEL_ALPHA, dirty_pixels, i, dirty_pixels);
		}
	}

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_DIRTY_TEXTURE]);

	return G_SOURCE_REMOVE;
}

/* PROPERTIES - GETTERS AND SETTERS */

int
ee_image_get_width (EeImage *self)
{
	if (! self->dirty_pixbuf)
		return 0;
	else
		return self->cached_width != CACHED_DEFAULT ? self->cached_width : gdk_pixbuf_get_width (self->dirty_pixbuf);
}

int
ee_image_get_height (EeImage *self)
{
	if (! self->dirty_pixbuf)
		return 0;
	else
		return self->cached_height != CACHED_DEFAULT ? self->cached_height : gdk_pixbuf_get_height (self->dirty_pixbuf);
}

void
ee_image_set_width (EeImage *self, int width)
{
	self->cached_width = width;

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_WIDTH]);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CLEAN_TEXTURE]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_DIRTY_TEXTURE]);
	ee_image_set_modified (self, TRUE);

}

void
ee_image_set_height (EeImage *self, int height)
{
	self->cached_height = height;

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_HEIGHT]);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CLEAN_TEXTURE]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_DIRTY_TEXTURE]);
	ee_image_set_modified (self, TRUE);

}

inline static GdkPixbuf *
create_scaled_and_rotated_pixbuf (EeImage *self, GdkPixbuf *pixbuf)
{
	int width = ee_image_get_width (self);
	int height = ee_image_get_height (self);
	g_autoptr(GdkPixbuf) scaled_pixbuf = gdk_pixbuf_scale_simple (pixbuf, width, height, GDK_INTERP_BILINEAR);
	GdkPixbuf *scaled_rotated_pixbuf;

	return scaled_rotated_pixbuf = gdk_pixbuf_rotate_simple (scaled_pixbuf, self->rotation);
}

/* transfer full */
GdkTexture *
ee_image_get_dirty_texture (EeImage *self)
{
	GdkTexture *texture;

	if (self->cached_height == CACHED_DEFAULT && self->cached_width == CACHED_DEFAULT && self->rotation == GDK_PIXBUF_ROTATE_NONE)
	{
		texture = gdk_texture_new_for_pixbuf (self->dirty_pixbuf);
	}
	else
	{
		g_autoptr(GdkPixbuf) scaled_rotated_dirty_pixbuf = create_scaled_and_rotated_pixbuf (self, self->dirty_pixbuf);

		texture = gdk_texture_new_for_pixbuf (scaled_rotated_dirty_pixbuf);
	}

	return texture;
}

/* transfer full */
GdkTexture *
ee_image_get_clean_texture (EeImage *self)
{
	GdkTexture *texture;

	if (self->cached_height == CACHED_DEFAULT && self->cached_width == CACHED_DEFAULT && self->rotation == GDK_PIXBUF_ROTATE_NONE)
	{
		texture = gdk_texture_new_for_pixbuf (self->clean_pixbuf);
	}
	else
	{
		g_autoptr(GdkPixbuf) scaled_rotated_clean_pixbuf = create_scaled_and_rotated_pixbuf (self, self->clean_pixbuf);

		texture = gdk_texture_new_for_pixbuf (scaled_rotated_clean_pixbuf);
	}

	return texture;
}

/* transfer full */
void
ee_image_set_file (EeImage *self, GFile *file)
{
	g_autoptr(GError) error = NULL;
	g_autoptr(GdkTexture) texture = NULL;
	g_autoptr(GdkPixbuf) pixbuf = NULL;

	if (!file) return;

	pixbuf = gdk_pixbuf_new_from_file (g_file_peek_path (file), &error);

	if (error) {
		ee_error_dialog (NULL, error);
		return;
	}

	g_clear_object (&self->file);
	self->file = g_object_ref (file);
	self->clean_pixbuf = g_steal_pointer (&pixbuf);
	self->dirty_pixbuf = gdk_pixbuf_copy (self->clean_pixbuf);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_FILE]);
}

/* transfer none */
GFile *
ee_image_get_file (EeImage *self)
{
	return self->file;
}

void
ee_image_set_brightness_rgba_mod (EeImage *self, EeRGBAMod *rgba_mod)
{
	self->brightness_rgba_mod = *rgba_mod;

	g_idle_add (refresh_dirty_pixbuf_rgb, self);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_BRIGHTNESS_RGBA_MOD]);
}

void
ee_image_get_brightness_rgba_mod (EeImage *self, EeRGBAMod *rgba_mod_out)
{
	*rgba_mod_out = self->brightness_rgba_mod;
}

void
ee_image_set_contrast_rgba_mod (EeImage *self, EeRGBAMod *rgba_mod)
{
	self->contrast_rgba_mod = *rgba_mod;

	g_idle_add (refresh_dirty_pixbuf_rgb, self);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONTRAST_RGBA_MOD]);
}

void
ee_image_get_contrast_rgba_mod (EeImage *self, EeRGBAMod *rgba_mod_out)
{
	*rgba_mod_out = self->contrast_rgba_mod;
}

void
ee_image_set_gamma_rgba_mod (EeImage *self, EeRGBAMod *rgba_mod)
{
	self->gamma_rgba_mod = *rgba_mod;

	g_idle_add (refresh_dirty_pixbuf_rgb, self);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CONTRAST_RGBA_MOD]);
}

void
ee_image_get_gamma_rgba_mod (EeImage *self, EeRGBAMod *rgba_mod_out)
{
	*rgba_mod_out = self->gamma_rgba_mod;
}

static void
ee_image_set_modified (EeImage *self, gboolean modified)
{
	self->modified = modified;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_MODIFIED]);
}

gboolean
ee_image_get_modified (EeImage *self)
{
	return self->modified;
}

void
ee_image_set_rotation (EeImage *self, GdkPixbufRotation rotation)
{
	self->rotation = rotation;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_ROTATION]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_DIRTY_TEXTURE]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CLEAN_TEXTURE]);
}

GdkPixbufRotation
ee_image_get_rotation (EeImage *self)
{
	return self->rotation;
}

static void
ee_image_set_property (GObject *object,
		guint property_id,
		const GValue *value,
		GParamSpec *pspec)
{
	EeImage *self = EE_IMAGE(object);

	switch (property_id)
	{
		case PROP_FILE:
			ee_image_set_file (self, g_value_get_object (value));
			break;

		case PROP_BRIGHTNESS_RGBA_MOD:
			ee_image_set_brightness_rgba_mod (self, g_value_get_boxed (value));
			break;

		case PROP_CONTRAST_RGBA_MOD:
			ee_image_set_contrast_rgba_mod (self, g_value_get_boxed (value));
			break;

		case PROP_GAMMA_RGBA_MOD:
			ee_image_set_gamma_rgba_mod (self, g_value_get_boxed (value));
			break;

		case PROP_MODIFIED:
			ee_image_set_modified (self, g_value_get_boolean (value));
			break;
			
		case PROP_WIDTH:
			ee_image_set_width (self, g_value_get_int (value));
			break;

		case PROP_HEIGHT:
			ee_image_set_height (self, g_value_get_int (value));
			break;

		case PROP_ROTATION:
			ee_image_set_rotation (self, g_value_get_enum (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
ee_image_get_property (GObject *object,
		guint property_id,
		GValue *value,
		GParamSpec *pspec)
{
	EeImage *self = EE_IMAGE(object);

	switch (property_id)
	{
		case PROP_FILE:
			g_value_set_object (value, ee_image_get_file (self));
			break;

		case PROP_DIRTY_TEXTURE:
			g_value_take_object (value, ee_image_get_dirty_texture (self));
			break;

		case PROP_CLEAN_TEXTURE:
			g_value_take_object (value, ee_image_get_clean_texture (self));
			break;

		case PROP_BRIGHTNESS_RGBA_MOD:
		{
			EeRGBAMod mod = {0};
			ee_image_get_brightness_rgba_mod (self, &mod);
			g_value_set_boxed (value, &mod);
		}
			break;

		case PROP_CONTRAST_RGBA_MOD:
		{
			EeRGBAMod mod = {0};
			ee_image_get_contrast_rgba_mod (self, &mod);
			g_value_set_boxed (value, &mod);
		}
			break;

		case PROP_GAMMA_RGBA_MOD:
		{
			EeRGBAMod mod = {0};
			ee_image_get_gamma_rgba_mod (self, &mod);
			g_value_set_boxed (value, &mod);
		}
			break;

		case PROP_MODIFIED:
			g_value_set_boolean (value, ee_image_get_modified (self));
			break;

		case PROP_WIDTH:
			g_value_set_int (value, ee_image_get_width (self));
			break;

		case PROP_HEIGHT:
			g_value_set_int (value, ee_image_get_height (self));
			break;

		case PROP_ROTATION:
			g_value_set_enum (value, ee_image_get_rotation (self));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/* METHOD DEFINITIONS */

static void
ee_image_init (EeImage *self)
{
	self->cached_height = CACHED_DEFAULT;
	self->cached_width = CACHED_DEFAULT;
}

static void
ee_image_dispose (GObject *object)
{
	EeImage *self = EE_IMAGE(object);

	g_clear_object (&self->file);

	/* Chain up */
	G_OBJECT_CLASS(ee_image_parent_class)->dispose (object);
}

static void
ee_image_finalize (GObject *object)
{
	EeImage *self = EE_IMAGE(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_image_parent_class)->finalize (object);
}

static void
ee_image_class_init (EeImageClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GParamFlags flags = G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY;

	object_class->dispose =  ee_image_dispose;
	object_class->finalize = ee_image_finalize;

	object_class->set_property = ee_image_set_property;
	object_class->get_property = ee_image_get_property;

	/* PROPERTIES */

	properties[PROP_FILE] = g_param_spec_object ("file", NULL, NULL,
			G_TYPE_FILE,
			flags | G_PARAM_CONSTRUCT);

	properties[PROP_DIRTY_TEXTURE] = g_param_spec_object ("dirty-texture", NULL, NULL,
			GDK_TYPE_TEXTURE,
			G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

	properties[PROP_CLEAN_TEXTURE] = g_param_spec_object ("clean-texture", NULL, NULL,
			GDK_TYPE_TEXTURE,
			G_PARAM_READABLE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY);

	properties[PROP_BRIGHTNESS_RGBA_MOD] = g_param_spec_boxed ("brightness-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			flags);

	properties[PROP_CONTRAST_RGBA_MOD] = g_param_spec_boxed ("contrast-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			flags);

	properties[PROP_GAMMA_RGBA_MOD] = g_param_spec_boxed ("gamma-rgba-mod", NULL, NULL,
			EE_TYPE_RGBA_MOD,
			flags);

	properties[PROP_MODIFIED] = g_param_spec_boolean ("modified", NULL, NULL,
			FALSE,
			flags);

	properties[PROP_WIDTH] = g_param_spec_int ("width", NULL, NULL,
			0, INT_MAX, 0,
			flags);

	properties[PROP_HEIGHT] = g_param_spec_int ("height", NULL, NULL,
			0, INT_MAX, 0,
			flags);

	properties[PROP_ROTATION] = g_param_spec_enum ("rotation", NULL, NULL,
			GDK_TYPE_PIXBUF_ROTATION,
			GDK_PIXBUF_ROTATE_NONE,
			flags);

	g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

/* PUBLIC METHOD DEFINITIONS */

/* file = can-null */
EeImage *
ee_image_new (GFile *file)
{
	return g_object_new (EE_TYPE_IMAGE,
			"file", file,
			NULL);
}

void
ee_image_clear_rgba_mods (EeImage *self)
{
	g_return_if_fail (EE_IS_IMAGE (self));

	ee_image_set_brightness_rgba_mod (self, &(EeRGBAMod){0,0,0,0});
	ee_image_set_contrast_rgba_mod (self, &(EeRGBAMod){0,0,0,0});
	ee_image_set_gamma_rgba_mod (self, &(EeRGBAMod){0,0,0,0});
}

/* FIXME - undo stack would be nice. But the O.G. ee didn't have that... */
void
ee_image_apply_changes (EeImage *self)
{
	g_return_if_fail (EE_IS_IMAGE (self));

	g_clear_object (&self->clean_pixbuf);
	self->clean_pixbuf = gdk_pixbuf_copy (self->dirty_pixbuf);

	ee_image_clear_rgba_mods (self);
	ee_image_set_modified (self, TRUE);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CLEAN_TEXTURE]);
}

void
ee_image_restore_original_size (EeImage *self)
{
	self->cached_height = CACHED_DEFAULT;
	self->cached_width = CACHED_DEFAULT;

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_WIDTH]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_HEIGHT]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_DIRTY_TEXTURE]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CLEAN_TEXTURE]);
}

void
ee_image_flip (EeImage *self, EeImageFlipType flippage)
{
	self->dirty_pixbuf = gdk_pixbuf_flip (self->dirty_pixbuf, flippage);
	self->clean_pixbuf = gdk_pixbuf_flip (self->clean_pixbuf, flippage);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_DIRTY_TEXTURE]);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_CLEAN_TEXTURE]);

	ee_image_set_modified (self, TRUE);
}

inline static void
apply_scaling_and_rotation_to_pixbufs (EeImage *self)
{
	self->clean_pixbuf = create_scaled_and_rotated_pixbuf (self, self->clean_pixbuf);
	self->dirty_pixbuf = create_scaled_and_rotated_pixbuf (self, self->dirty_pixbuf);

	ee_image_restore_original_size (self);
}

gboolean
ee_image_get_crop_dimensions (EeImage *self, GtkWidget *picture_widget, GtkWidget *parent_widget, GdkRectangle *rect)
{
	graphene_rect_t bounds = {0};
	gboolean can_compute_bounds = gtk_widget_compute_bounds (picture_widget, parent_widget, &bounds);

	g_return_val_if_fail (can_compute_bounds, FALSE);

	{
		const int frame_width = gtk_widget_get_width (parent_widget);
		const int frame_height = gtk_widget_get_height (parent_widget);
		const int width = MIN (bounds.size.width, frame_width);
		const int height = MIN (bounds.size.height, frame_height);

		g_return_val_if_fail (width > 0 && height > 0, FALSE);

		rect->width = width;
		rect->height = height;
		rect->x = width < frame_width ? 0 : ABS (bounds.origin.x);
		rect->y = height < frame_height ? 0 : ABS (bounds.origin.y);
	}

	return TRUE;
}

void
ee_image_crop (EeImage *self, GtkWidget *picture_widget, GtkWidget *parent_widget)
{
	GdkRectangle rect = {0};
	gboolean retval = ee_image_get_crop_dimensions (self, picture_widget, parent_widget, &rect);

	if (!retval) {
		g_critical ("%s: Unable to get crop dimensions", __func__);
		return;
	}

	apply_scaling_and_rotation_to_pixbufs (self);

	self->dirty_pixbuf = gdk_pixbuf_new_subpixbuf (self->dirty_pixbuf, rect.x, rect.y, rect.width, rect.height);
	self->clean_pixbuf = gdk_pixbuf_new_subpixbuf (self->clean_pixbuf, rect.x, rect.y, rect.width, rect.height);

	ee_image_restore_original_size (self);
	ee_image_set_modified (self, TRUE);
}

static void
add_if_writable (GdkPixbufFormat *data, GSList **list)
{
  if (gdk_pixbuf_format_is_writable (data))
    *list = g_slist_prepend (*list, data);
}

/* new_filename = can-NULL. If it is NULL, the pre-existing filename will be
 * used. In other words, use NULL for 'save' and non-NULL for 'save as'.
 */
gboolean
ee_image_save (EeImage *self, const char *new_filename, GError **error)
{
	const char *filename = new_filename ? new_filename : g_file_peek_path (self->file);
	gboolean retval;
	g_autoptr(GSList) formats = NULL;
	g_autoptr(GSList) writable_formats = NULL;
	g_autofree char *extension = NULL;
	g_autofree char *format_to_write_to = NULL;
	g_autoptr(GFile) new_gfile = NULL;

	g_return_val_if_fail (G_IS_FILE (self->file), FALSE);

	extension = get_extension (filename);
	formats = gdk_pixbuf_get_formats ();
	g_slist_foreach (formats, (GFunc)add_if_writable, &writable_formats);

	for (GSList *l = writable_formats; l; l = l->next)
	{
		GdkPixbufFormat *fmt_iter = l->data;
		g_autofree char *fmt_name = gdk_pixbuf_format_get_name (fmt_iter);
		const char *ext_fmt = img_extension_lookup (extension);

		if (g_strcmp0 (ext_fmt, fmt_name) == 0)
			format_to_write_to = g_steal_pointer (&fmt_name);
	}

	if (! format_to_write_to)
	{
		g_set_error (error, G_IO_ERROR, G_IO_ERROR_NOT_SUPPORTED, _("Image format not supported: %s"), extension);
		return FALSE;
	}

	retval = gdk_pixbuf_save (self->dirty_pixbuf, filename, format_to_write_to, error, NULL);

	if (! retval)
		return FALSE;

	/* Refresh image based on new file or newly updated pre-existing file */
	new_gfile = g_file_new_for_path (filename);
	ee_image_set_file (self, new_gfile);
	ee_image_set_modified (self, FALSE);

	return TRUE;
}

/* transfer none */
GdkPixbuf *
ee_image_get_dirty_pixbuf (EeImage *self)
{
	return self->dirty_pixbuf;
}
