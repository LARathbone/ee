// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#pragma once

#include <gtk/gtk.h>

#include "ee-util.h"

void ee_print (EeImage *image);
