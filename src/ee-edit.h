// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#pragma once

#include <gtk/gtk.h>
#include <libportal-gtk4/portal-gtk4.h>

#include "ee-image.h"
#include "ee-graph.h"
#include "ee-application.h"
#include "ee-application-window.h"
#include "ee-util.h"

G_BEGIN_DECLS

/* Type declaration */

#define EE_TYPE_EDIT ee_edit_get_type()
G_DECLARE_FINAL_TYPE (EeEdit, ee_edit, EE, EDIT, GtkWindow)

/* Method declarations */

GtkWidget *	ee_edit_new (void);
gboolean ee_edit_get_always_apply (EeEdit *self);

G_END_DECLS
