// vim: ts=4 sw=4 linebreak breakindent breakindentopt=shift\:8

#include "ee-application.h"

#include <glib/gi18n.h>

// TEST
#define GETTEXT_PACKAGE "electric-eyes"
#define LOCALEDIR "/usr/share/locale"

static void
setup_i18n (void)
{
  bindtextdomain (GETTEXT_PACKAGE, LOCALEDIR);
  bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
  textdomain (GETTEXT_PACKAGE);
}

int
main (int argc, char *argv[])
{
	g_autoptr(EeApplication) app = NULL;
	int status;

	setup_i18n ();

	app = ee_application_new ();
	status = g_application_run (G_APPLICATION(app), argc, argv);

	return status;
}
