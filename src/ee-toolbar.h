#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

/* Type declaration */

#define EE_TYPE_TOOLBAR ee_toolbar_get_type()
G_DECLARE_FINAL_TYPE (EeToolbar, ee_toolbar, EE, TOOLBAR, GtkWidget)

/* Method declarations */

GtkWidget *	ee_toolbar_new (void);

G_END_DECLS
