// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#include "ee-application.h"

#include "ee-edit.h"
#include "ee-list.h"
#include "ee-print.h"

#include "ee-enums.h"
#include <glib/gi18n.h>

/* PROPERTIES */

enum
{
	PROP_SHOW_EDIT_WINDOW = 1,
	PROP_SHOW_LIST_WINDOW,
	PROP_SETTINGS,
	N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

/* GOBJECT DEFINITION */

struct _EeApplication
{
	GtkApplication parent_instance;

	GtkWidget *image_view_window;
	GtkWidget *edit_window;
	GtkWidget *list_window;

	GSettings *settings;
};

G_DEFINE_TYPE (EeApplication, ee_application, GTK_TYPE_APPLICATION)

/* PRIVATE METHODS AND HELPERS */

G_GNUC_BEGIN_IGNORE_DEPRECATIONS
static void
open_chooser_response_cb (EeApplication *self, int response_id, GtkNativeDialog *chooser)
{
	switch (response_id)
	{
		case GTK_RESPONSE_ACCEPT:
		{
			g_autoptr(GListModel) files = gtk_file_chooser_get_files (GTK_FILE_CHOOSER(chooser));

			for (guint i = 0; i < g_list_model_get_n_items (files); ++i)
			{
				g_autoptr(GFile) file = g_list_model_get_item (files, i);
				ee_application_open_file (self, file);
			}
		}
			break;

		default:
			g_debug ("%s: No file selected", __func__);
			break;
	}
	g_object_unref (chooser);
}

static void
open_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);
	GtkFileChooserNative *chooser = gtk_file_chooser_native_new (_("Open File"), GTK_WINDOW(self->image_view_window), GTK_FILE_CHOOSER_ACTION_OPEN, _("Open"), _("Cancel"));

	gtk_file_chooser_set_select_multiple (GTK_FILE_CHOOSER(chooser), TRUE);

	g_signal_connect_swapped (chooser, "response", G_CALLBACK(open_chooser_response_cb), self);

	gtk_native_dialog_show (GTK_NATIVE_DIALOG(chooser));
}
G_GNUC_END_IGNORE_DEPRECATIONS

inline static void
save_action_common (EeApplication *self, const char *maybe_filename)
{
	EeImage *image = ee_application_window_get_image (EE_APPLICATION_WINDOW(self->image_view_window));
	g_autoptr(GError) local_error = NULL;
	gboolean retval = ee_image_save (image, maybe_filename, &local_error);

	if (! retval)
		ee_error_dialog (NULL, local_error);
}

G_GNUC_BEGIN_IGNORE_DEPRECATIONS
static void
save_as_dialog_response_cb (EeApplication *self, int response_id, GtkNativeDialog *native)
{
	switch (response_id)
	{
		case GTK_RESPONSE_ACCEPT:
		{
			EeImage *image = ee_application_window_get_image (EE_APPLICATION_WINDOW(self->image_view_window));
			g_autoptr(GFile) file = gtk_file_chooser_get_file (GTK_FILE_CHOOSER(native));

			save_action_common (self, g_file_peek_path (file));
		}
			break;

		default:
			break;
	}
	g_object_unref (native);
}

static void
save_as_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);
	GtkFileChooserNative *native = gtk_file_chooser_native_new (_("Save As"), GTK_WINDOW(self->image_view_window), GTK_FILE_CHOOSER_ACTION_SAVE, _("Save"), _("Cancel"));
	GtkFileFilter *filter = gtk_file_filter_new ();

	gtk_file_filter_add_pixbuf_formats (filter);
	gtk_file_chooser_set_filter (GTK_FILE_CHOOSER(native), filter);
	g_signal_connect_swapped (native, "response", G_CALLBACK(save_as_dialog_response_cb), self);
	gtk_native_dialog_show (GTK_NATIVE_DIALOG(native));
}
G_GNUC_END_IGNORE_DEPRECATIONS

static void
save_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);
	EeImage *image = ee_application_window_get_image (ee_application_get_application_window (self));

	save_action_common (self, NULL);
}

static void
have_unsaved_accept_or_reject_cb (GtkWindow *dialog, int response_id, gpointer user_data)
{
	switch (response_id)
	{
		case GTK_RESPONSE_ACCEPT:
			g_application_quit (g_application_get_default ());
			break;

		case GTK_RESPONSE_REJECT:
		default:
			gtk_window_destroy (GTK_WINDOW(dialog));
			break;
	}
}

static void
print_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);
	EeImage *image = ee_application_window_get_image (EE_APPLICATION_WINDOW(self->image_view_window));

	g_return_if_fail (EE_IS_IMAGE (image));

	ee_print (image);
}

static void
save_settings_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);

	g_settings_set_int (self->settings, "window-width", gtk_widget_get_width (self->image_view_window));
	g_settings_set_int (self->settings, "window-height", gtk_widget_get_height (self->image_view_window));
	g_settings_set_boolean (self->settings, "scrolled-view", ee_application_window_get_scrolled_view (EE_APPLICATION_WINDOW(self->image_view_window)));
	g_settings_set_boolean (self->settings, "show-toolbar", ee_application_window_get_show_toolbar (EE_APPLICATION_WINDOW(self->image_view_window)));
	g_settings_set_enum (self->settings, "toolbar-position", ee_application_window_get_toolbar_pos (EE_APPLICATION_WINDOW(self->image_view_window)));
	g_settings_set_boolean (self->settings, "generate-thumbnails", ee_list_get_gen_thumbnails (EE_LIST(self->list_window)));
	g_settings_set_boolean (self->settings, "use-large-thumbnails", ee_list_get_use_large_thumbnails (EE_LIST(self->list_window)));
	g_settings_set_boolean (self->settings, "always-apply", ee_edit_get_always_apply (EE_EDIT(self->edit_window)));
}

static void
quit_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);
	gboolean have_unsaved = FALSE;
	GListModel *model = ee_list_get_model (EE_LIST(self->list_window));

	for (guint i = 0; i < g_list_model_get_n_items (model); ++i)
	{
		EeListData *data = g_list_model_get_item (model, i);
		EeImage *image = ee_list_data_get_image (data);

		if (ee_image_get_modified (image))
		{
			have_unsaved = TRUE;
			break;
		}
	}

	if (have_unsaved)
	{
		ee_accept_or_reject_dialog (NULL,
				_("You have one or more images with unsaved changes.\n\n"
					"What would you like to do?"),
				_("_Quit Anyway"),
				_("_Go Back"),
				have_unsaved_accept_or_reject_cb,
				NULL);
	}
	else
	{
		g_application_quit (G_APPLICATION(self));
	}
}

static void
first_image_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);
	GtkSingleSelection *selection = GTK_SINGLE_SELECTION(ee_list_get_selection (EE_LIST(self->list_window)));

	gtk_single_selection_set_selected (selection, 0);
}

static void
prev_image_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);
	GtkSingleSelection *selection = GTK_SINGLE_SELECTION(ee_list_get_selection (EE_LIST(self->list_window)));
	guint selected = gtk_single_selection_get_selected (selection);
	guint prev;

	prev = selected == 0 ? g_list_model_get_n_items (G_LIST_MODEL (selection)) - 1 : selected - 1;

	gtk_single_selection_set_selected (GTK_SINGLE_SELECTION(selection), prev);
}

static void
next_image_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);
	GtkSingleSelection *selection = GTK_SINGLE_SELECTION(ee_list_get_selection (EE_LIST(self->list_window)));
	guint selected = gtk_single_selection_get_selected (selection);
	guint next;

	next = selected == g_list_model_get_n_items (G_LIST_MODEL(selection)) - 1 ? 0 : selected + 1;

	gtk_single_selection_set_selected (selection, next);
}

static void
last_image_action (GSimpleAction *action, GVariant *parameter, gpointer user_data)
{
	EeApplication *self = EE_APPLICATION(user_data);
	GtkSingleSelection *selection = GTK_SINGLE_SELECTION(ee_list_get_selection (EE_LIST(self->list_window)));

	gtk_single_selection_set_selected (selection, g_list_model_get_n_items (G_LIST_MODEL(selection)) - 1);
}

inline static void
setup_accels (EeApplication *self)
{
	/* YUCK! GTK team, *please* improve this API for GTK5... */

	struct {
		const char *action_and_target;
		const char *accelerators[2];
	} accels[] = {
		{ "app.open", { "<Control>o", NULL } },
		{ "app.show-edit-window", { "<Control>e", NULL } },
		{ "app.show-list-window", { "<Control>l", NULL } },
		{ "app.quit", { "<Control>q", NULL } },
	};

	for (int i = 0; i < G_N_ELEMENTS (accels); i++)
		gtk_application_set_accels_for_action (GTK_APPLICATION(self), accels[i].action_and_target, accels[i].accelerators);
}

static void
image_nav_actions_enable_cb (GListModel *model, guint position, guint removed, guint added, GAction *action)
{
	g_simple_action_set_enabled (G_SIMPLE_ACTION(action), g_list_model_get_n_items (model) > 1 ? TRUE : FALSE);
}

inline static void
setup_actions (EeApplication *self)
{
	g_autoptr(GPropertyAction) app_show_edit = NULL;
	g_autoptr(GPropertyAction) app_show_list = NULL;

	const GActionEntry actions[] = {
		{ "open", open_action },
		{ "save", save_action},
		{ "save-as", save_as_action},
		{ "first-image", first_image_action},
		{ "prev-image", prev_image_action},
		{ "next-image", next_image_action},
		{ "last-image", last_image_action},
		{ "print", print_action },
		{ "save-settings", save_settings_action},
		{ "quit", quit_action }
	};
	const char *image_nav_actions[] = {"first-image", "prev-image", "next-image", "last-image"};

	g_action_map_add_action_entries (G_ACTION_MAP(self), actions, G_N_ELEMENTS (actions), self);

	app_show_edit = g_property_action_new ("show-edit-window", self, "show-edit-window");
	g_action_map_add_action (G_ACTION_MAP(self), G_ACTION(app_show_edit));

	app_show_list = g_property_action_new ("show-list-window", self, "show-list-window");
	g_action_map_add_action (G_ACTION_MAP(self), G_ACTION(app_show_list));

	/* Enable/disable image navigation actions based on whether there's > 1 image in the list
	 */
	for (int i = 0; i < G_N_ELEMENTS (image_nav_actions); ++i)
	{
		GAction *action = g_action_map_lookup_action (G_ACTION_MAP(self), image_nav_actions[i]);
		GListModel *selection = G_LIST_MODEL(ee_list_get_selection (EE_LIST(self->list_window)));

		g_simple_action_set_enabled (G_SIMPLE_ACTION(action), FALSE);
		g_signal_connect (selection, "items-changed", G_CALLBACK(image_nav_actions_enable_cb), action);
	}

	/* Bind saveability to modified state of image */
	{
		GAction *save_gaction = g_action_map_lookup_action (G_ACTION_MAP(self), "save");
		GAction *save_as_gaction = g_action_map_lookup_action (G_ACTION_MAP(self), "save-as");
		GtkExpression *appwin_image_expr = gtk_property_expression_new (EE_TYPE_APPLICATION_WINDOW, NULL, "image");
		GtkExpression *image_modified_expr = gtk_property_expression_new (EE_TYPE_IMAGE, appwin_image_expr, "modified");

		g_simple_action_set_enabled (G_SIMPLE_ACTION(save_gaction), FALSE);
		g_simple_action_set_enabled (G_SIMPLE_ACTION(save_as_gaction), FALSE);

		gtk_expression_bind (gtk_expression_ref (image_modified_expr), save_gaction, "enabled", self->image_view_window);
		gtk_expression_bind (image_modified_expr, save_as_gaction, "enabled", self->image_view_window);
	}

	/* Bind printability to existence of an image in the appwin */
	{
		GAction *print_gaction = g_action_map_lookup_action (G_ACTION_MAP(self), "print");

		g_object_bind_property_full (self->image_view_window, "image", print_gaction, "enabled", G_BINDING_SYNC_CREATE, ee_have_object_transform_to, NULL, NULL, NULL);
	}
}

/* NOTE: We don't actually allow the showing of the menubar in the UI right
 * now, but it's nice to have this set up in the event that we want to create
 * an option in the future.
 */
inline static void
setup_menubar (EeApplication *self)
{
	g_autoptr(GtkBuilder) builder = gtk_builder_new_from_resource ("/com/gitlab/LARathbone/EE/ee-main-menu.ui");
	GMenuModel *menu_model = (GMenuModel *) gtk_builder_get_object (builder, "main_menu");

	gtk_application_set_menubar (GTK_APPLICATION(self), menu_model);
}

inline static void
setup_initial_image_view_window (EeApplication *self)
{
	self->image_view_window = ee_application_window_new ();
	gtk_application_add_window (GTK_APPLICATION(self), GTK_WINDOW(self->image_view_window));
	gtk_window_present (GTK_WINDOW(self->image_view_window));
}

static gboolean
list_selection_transform_to (GBinding *binding,
		const GValue *from_value /* source = guint */,
		GValue *to_value /* target = EeImage */,
		EeApplication *self)
{
	GtkSingleSelection *selection = GTK_SINGLE_SELECTION(ee_list_get_selection (EE_LIST(self->list_window)));
	guint pos = g_value_get_uint (from_value);
	EeListData *data = NULL;
	EeImage *image = NULL;

	data = g_list_model_get_item (G_LIST_MODEL(selection), pos);
	if (!data) goto out;

	image = ee_list_data_get_image (data);
	if (!image) goto out;

out:
	g_value_set_object (to_value, image);
	return TRUE;
}

static gboolean
list_selection_transform_from (GBinding *binding,
		const GValue *from_value /* target = EeImage */,
		GValue *to_value /* source = guint */,
		EeApplication *self)
{
	EeImage *image = g_value_get_object (from_value);
	GtkSingleSelection *selection = GTK_SINGLE_SELECTION(ee_list_get_selection (EE_LIST(self->list_window)));

	for (guint i = 0; i < g_list_model_get_n_items (G_LIST_MODEL(selection)); ++i)
	{
		EeListData *data_iter = g_list_model_get_item (G_LIST_MODEL(selection), i);
		EeImage *image_iter = ee_list_data_get_image (data_iter);

		if (image == image_iter) {
			g_value_set_uint (to_value, i);
			return TRUE;
		}
	}
	return FALSE;
}

inline static void
setup_bindings (EeApplication *self)
{
	GtkSelectionModel *selection;

	g_object_bind_property (self->image_view_window, "image", self->edit_window, "image", G_BINDING_SYNC_CREATE);

	g_object_bind_property_full (self->image_view_window, "image", self->edit_window, "sensitive",
			G_BINDING_SYNC_CREATE,
			ee_have_object_transform_to,
			NULL,
			NULL,
			NULL);

	selection = ee_list_get_selection (EE_LIST(self->list_window));
	g_object_bind_property_full (selection, "selected", self->image_view_window, "image",
			G_BINDING_BIDIRECTIONAL,
			(GBindingTransformFunc) list_selection_transform_to,
			(GBindingTransformFunc) list_selection_transform_from,
			self,
			NULL);
}

inline static void
setup_css (EeApplication *self)
{
	GtkCssProvider *provider = gtk_css_provider_new ();

	gtk_css_provider_load_from_resource (provider, "/com/gitlab/LARathbone/EE/css/ee.css");
	gtk_style_context_add_provider_for_display (gdk_display_get_default (),
			GTK_STYLE_PROVIDER (provider),
			GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);
}

inline static void
setup_settings (EeApplication *self)
{
	self->settings = g_settings_new ("com.gitlab.LARathbone.EE");

	g_settings_bind (self->settings, "window-width", self->image_view_window, "default-width", G_SETTINGS_BIND_GET | G_SETTINGS_BIND_GET_NO_CHANGES);
	g_settings_bind (self->settings, "window-height", self->image_view_window, "default-height", G_SETTINGS_BIND_GET | G_SETTINGS_BIND_GET_NO_CHANGES);
	g_settings_bind (self->settings, "scrolled-view", self->image_view_window, "scrolled-view",G_SETTINGS_BIND_GET | G_SETTINGS_BIND_GET_NO_CHANGES);
	g_settings_bind (self->settings, "show-toolbar", self->image_view_window, "show-toolbar", G_SETTINGS_BIND_GET | G_SETTINGS_BIND_GET_NO_CHANGES);
	g_settings_bind (self->settings, "toolbar-position", self->image_view_window, "toolbar-position", G_SETTINGS_BIND_GET | G_SETTINGS_BIND_GET_NO_CHANGES);
	g_settings_bind (self->settings, "generate-thumbnails", self->list_window, "generate-thumbnails", G_SETTINGS_BIND_GET | G_SETTINGS_BIND_GET_NO_CHANGES);
	g_settings_bind (self->settings, "use-large-thumbnails", self->list_window, "use-large-thumbnails", G_SETTINGS_BIND_GET | G_SETTINGS_BIND_GET_NO_CHANGES);
	g_settings_bind (self->settings, "always-apply", self->edit_window, "always-apply", G_SETTINGS_BIND_GET | G_SETTINGS_BIND_GET_NO_CHANGES);
}

static void
ee_application_activate (GApplication *app)
{
	EeApplication *self = EE_APPLICATION(app);

	setup_css (self);
	setup_menubar (self);
	setup_initial_image_view_window (self);

	self->edit_window = ee_edit_new ();
	self->list_window = ee_list_new ();

	setup_actions (self);
	setup_accels (self);

	gtk_application_add_window (GTK_APPLICATION(app), GTK_WINDOW(self->list_window));
	gtk_application_add_window (GTK_APPLICATION(app), GTK_WINDOW(self->edit_window));

	setup_bindings (self);

	/* Setup settings fairly late in the process because we need to make sure
	 * our appwin is instantiated first
	 */
	setup_settings (self);

	/* Chain up */
	G_APPLICATION_CLASS(ee_application_parent_class)->activate (app);
}

static void
ee_application_open (GApplication *app, GFile *files[], int n_files, const char *hint)
{
	EeApplication *self = EE_APPLICATION(app);

	ee_application_activate (app);

	for (int i = 0; i < n_files; ++i)
		ee_application_open_file (self, files[i]);
}

/* PROPERTIES - GETTERS AND SETTERS */

GSettings *
ee_application_get_settings (EeApplication *self)
{
	return self->settings;
}

void
ee_application_set_show_edit_window (EeApplication *self, gboolean show)
{
	if (!self->edit_window) {
		g_warning ("%s: Trying to set the show-edit-window property but the edit window doesn't exist yet", __func__);
		return;
	}

	gtk_widget_set_visible (self->edit_window, show);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_SHOW_EDIT_WINDOW]);
}

void
ee_application_set_show_list_window (EeApplication *self, gboolean show)
{
	if (!self->list_window) {
		g_warning ("%s: Trying to set the show-list-window property but the list window doesn't exist yet", __func__);
		return;
	}

	gtk_widget_set_visible (self->list_window, show);

	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_SHOW_LIST_WINDOW]);
}

gboolean
ee_application_get_show_edit_window (EeApplication *self)
{
	if (self->edit_window)
		return gtk_widget_get_visible (self->edit_window);
	else
		return FALSE;
}

gboolean
ee_application_get_show_list_window (EeApplication *self)
{
	if (self->list_window)
		return gtk_widget_get_visible (self->list_window);
	else
		return FALSE;
}

static void
ee_application_set_property (GObject *object,
		guint property_id,
		const GValue *value,
		GParamSpec *pspec)
{
	EeApplication *self = EE_APPLICATION(object);

	switch (property_id)
	{
		case PROP_SHOW_EDIT_WINDOW:
			ee_application_set_show_edit_window (self, g_value_get_boolean (value));
			break;

		case PROP_SHOW_LIST_WINDOW:
			ee_application_set_show_list_window (self, g_value_get_boolean (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
ee_application_get_property (GObject *object,
		guint property_id,
		GValue *value,
		GParamSpec *pspec)
{
	EeApplication *self = EE_APPLICATION(object);

	switch (property_id)
	{
		case PROP_SHOW_EDIT_WINDOW:
			g_value_set_boolean (value, ee_application_get_show_edit_window (self));
			break;

		case PROP_SHOW_LIST_WINDOW:
			g_value_set_boolean (value, ee_application_get_show_list_window (self));
			break;

		case PROP_SETTINGS:
			g_value_set_object (value, ee_application_get_settings (self));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/* METHOD DEFINITIONS */

static void
ee_application_init (EeApplication *self)
{
}

static void
ee_application_constructed (GObject *object)
{
	EeApplication *self = EE_APPLICATION(object);

	/* This needs to be done in _constructed for some reason. (Read: I tried it in _init and it didn't work; so I decided to copy Christian Hergert's code). */
	g_application_set_application_id (G_APPLICATION(self), "com.gitlab.LARathbone.EE");

	/* Chain up */
	G_OBJECT_CLASS(ee_application_parent_class)->constructed (object);
}

static void
ee_application_dispose (GObject *object)
{
	EeApplication *self = EE_APPLICATION(object);

	g_clear_object (&self->settings);

	/* Chain up */
	G_OBJECT_CLASS(ee_application_parent_class)->dispose (object);
}

static void
ee_application_finalize (GObject *object)
{
	EeApplication *self = EE_APPLICATION(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_application_parent_class)->finalize (object);
}

static void
ee_application_class_init (EeApplicationClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GApplicationClass *app_class = G_APPLICATION_CLASS(klass);
	GParamFlags flags = G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY;

	object_class->constructed = ee_application_constructed;
	object_class->dispose =  ee_application_dispose;
	object_class->finalize = ee_application_finalize;
	object_class->set_property = ee_application_set_property;
	object_class->get_property = ee_application_get_property;

	app_class->activate = ee_application_activate;
	app_class->open = ee_application_open;

	/* PROPERTIES */

	properties[PROP_SHOW_EDIT_WINDOW] = g_param_spec_boolean ("show-edit-window", NULL, NULL,
				FALSE,
				flags);

	properties[PROP_SHOW_LIST_WINDOW] = g_param_spec_boolean ("show-list-window", NULL, NULL,
				FALSE,
				flags);

	properties[PROP_SETTINGS] = g_param_spec_object ("settings", NULL, NULL,
				G_TYPE_SETTINGS,
				G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

/* PUBLIC METHOD DEFINITIONS */

EeApplication *
ee_application_new (void)
{
	return g_object_new (EE_TYPE_APPLICATION,
			"flags", G_APPLICATION_HANDLES_OPEN,
			NULL);
}

/* transfer none */
void
ee_application_open_file (EeApplication *self, GFile *file)
{
	g_autoptr(EeImage) image = NULL;

	g_return_if_fail (EE_IS_APPLICATION (self));

	image = ee_image_new (file);

	if (!ee_image_get_file (image))
		return;

	/* This will also set the image in the viewer window. */
	ee_list_add_entry (EE_LIST(self->list_window), g_steal_pointer (&image)); 
}

EeApplicationWindow *
ee_application_get_application_window (EeApplication *self)
{
	return EE_APPLICATION_WINDOW(self->image_view_window);
}

void
ee_application_quit (EeApplication *self)
{
	GAction *action = g_action_map_lookup_action (G_ACTION_MAP(self), "quit");

	g_action_activate (action, NULL);
}
