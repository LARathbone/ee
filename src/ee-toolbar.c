#include "ee-toolbar.h"

/* PROPERTIES */

enum
{
	PROP_ORIENTATION = 1,
	N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

/* GOBJECT DEFINITION */

struct _EeToolbar
{
	GtkWidget parent_instance;
};

G_DEFINE_TYPE_WITH_CODE (EeToolbar, ee_toolbar, GTK_TYPE_WIDGET,
		G_IMPLEMENT_INTERFACE (GTK_TYPE_ORIENTABLE, NULL));

/* PROPERTIES - GETTERS AND SETTERS */

static void
ee_toolbar_set_property (GObject *object,
		guint property_id,
		const GValue *value,
		GParamSpec *pspec)
{
	EeToolbar *self = EE_TOOLBAR(object);

	switch (property_id)
	{
		case PROP_ORIENTATION:
			gtk_orientable_set_orientation (GTK_ORIENTABLE(gtk_widget_get_layout_manager (GTK_WIDGET(self))), g_value_get_enum (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
ee_toolbar_get_property (GObject *object,
		guint property_id,
		GValue *value,
		GParamSpec *pspec)
{
	EeToolbar *self = EE_TOOLBAR(object);

	switch (property_id)
	{
		case PROP_ORIENTATION:
			g_value_set_enum (value, gtk_orientable_get_orientation (GTK_ORIENTABLE(gtk_widget_get_layout_manager (GTK_WIDGET(self)))));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/* METHOD DEFINITIONS */

static void
ee_toolbar_init (EeToolbar *self)
{
	gtk_widget_init_template (GTK_WIDGET(self));
}

static void
ee_toolbar_dispose (GObject *object)
{
	EeToolbar *self = EE_TOOLBAR(object);
	GtkWidget *child;

	while ((child = gtk_widget_get_first_child (GTK_WIDGET(self))) != NULL)
		gtk_widget_unparent (child);

	/* Chain up */
	G_OBJECT_CLASS(ee_toolbar_parent_class)->dispose (object);
}

static void
ee_toolbar_finalize (GObject *object)
{
	EeToolbar *self = EE_TOOLBAR(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_toolbar_parent_class)->finalize (object);
}

static void
ee_toolbar_class_init (EeToolbarClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);

	object_class->dispose =  ee_toolbar_dispose;
	object_class->finalize = ee_toolbar_finalize;

	object_class->set_property = ee_toolbar_set_property;
	object_class->get_property = ee_toolbar_get_property;

	/* PROPERTIES */

	g_object_class_override_property (object_class, PROP_ORIENTATION, "orientation");

	/* TEMPLATE */
	gtk_widget_class_set_template_from_resource (widget_class, "/com/gitlab/LARathbone/EE/ee-toolbar.ui");
}

/* PUBLIC METHOD DEFINITIONS */

GtkWidget *
ee_toolbar_new (void)
{
	return g_object_new (EE_TYPE_TOOLBAR, NULL);
}
