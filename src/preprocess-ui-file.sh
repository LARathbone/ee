#!/bin/bash

if [ -z "$1" ] || [ -z "$2" ]; then
    echo "Usage: $0 <input-file> <output-file>"
    exit 1
fi

pwd
cpp -I . -P "$1" | sed '1,/__MARK__/d' > "$2"
