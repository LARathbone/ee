#pragma once

#include <gtk/gtk.h>

#include "ee-image.h"

G_BEGIN_DECLS

/* Type declaration */

#define EE_TYPE_LIST_DATA ee_list_data_get_type()
G_DECLARE_FINAL_TYPE (EeListData, ee_list_data, EE, LIST_DATA, GObject)

/* Method declarations */

EeListData *	ee_list_data_new (EeImage *image);
EeImage * ee_list_data_get_image (EeListData *self);

G_END_DECLS
