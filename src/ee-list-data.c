// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#include "ee-list-data.h"

/* PROPERTIES */

enum
{
	PROP_0,
	PROP_IMAGE,
	PROP_THUMBNAIL,
	PROP_SHOW_THUMBNAIL,
	PROP_THUMBNAIL_SIZE,
	N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

/* GOBJECT DEFINITION */

struct _EeListData
{
	GObject parent_instance;

	EeImage *image;
	GdkTexture *thumbnail;
	GtkIconSize thumbnail_size;
	gboolean show_thumbnail;
};

G_DEFINE_TYPE (EeListData, ee_list_data, G_TYPE_OBJECT)

/* PROPERTIES - GETTERS AND SETTERS */

/* transfer none */
void
ee_list_data_set_thumbnail (EeListData *self, GdkTexture *texture)
{
	if (!texture)
		return;

	self->thumbnail = g_object_ref (texture);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_THUMBNAIL]);
}

/* transfer none */
GdkTexture *
ee_list_data_get_thumbnail (EeListData *self)
{
	return self->thumbnail;
}

void
ee_list_data_set_thumbnail_size (EeListData *self, GtkIconSize size)
{
	self->thumbnail_size = size;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_THUMBNAIL_SIZE]);
}

GtkIconSize
ee_list_data_get_thumbnail_size (EeListData *self)
{
	return self->thumbnail_size;
}

void
ee_list_data_set_show_thumbnail (EeListData *self, gboolean show)
{
	self->show_thumbnail = show;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_SHOW_THUMBNAIL]);
}

gboolean
ee_list_data_get_show_thumbnail (EeListData *self)
{
	return self->show_thumbnail;
}

EeImage *
ee_list_data_get_image (EeListData *self)
{
	return self->image;
}

static void
ee_list_data_set_image (EeListData *self, EeImage *image)
{
	self->image = image;
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_IMAGE]);
}

static void
ee_list_data_set_property (GObject *object,
		guint property_id,
		const GValue *value,
		GParamSpec *pspec)
{
	EeListData *self = EE_LIST_DATA(object);

	switch (property_id)
	{
		case PROP_IMAGE:
			ee_list_data_set_image (self, g_value_get_object (value));
			break;

		case PROP_THUMBNAIL:
			ee_list_data_set_thumbnail (self, g_value_get_object (value));
			break;

		case PROP_SHOW_THUMBNAIL:
			ee_list_data_set_show_thumbnail (self, g_value_get_boolean (value));
			break;

		case PROP_THUMBNAIL_SIZE:
			ee_list_data_set_thumbnail_size (self, g_value_get_enum (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
ee_list_data_get_property (GObject *object,
		guint property_id,
		GValue *value,
		GParamSpec *pspec)
{
	EeListData *self = EE_LIST_DATA(object);

	switch (property_id)
	{
		case PROP_IMAGE:
			g_value_set_object (value, ee_list_data_get_image (self));
			break;

		case PROP_THUMBNAIL:
			g_value_set_object (value, ee_list_data_get_thumbnail (self));
			break;

		case PROP_SHOW_THUMBNAIL:
			g_value_set_boolean (value, ee_list_data_get_show_thumbnail (self));
			break;

		case PROP_THUMBNAIL_SIZE:
			g_value_set_enum (value, ee_list_data_get_thumbnail_size (self));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/* METHOD DEFINITIONS */

static void
ee_list_data_init (EeListData *self)
{
}

static void
ee_list_data_dispose (GObject *object)
{
	EeListData *self = EE_LIST_DATA(object);

	g_clear_object (&self->image);

	/* Chain up */
	G_OBJECT_CLASS(ee_list_data_parent_class)->dispose (object);
}

static void
ee_list_data_finalize (GObject *object)
{
	EeListData *self = EE_LIST_DATA(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_list_data_parent_class)->finalize (object);
}

static void
ee_list_data_constructed (GObject *object)
{
	EeListData *self = EE_LIST_DATA(object);

	g_object_bind_property (self->image, "clean-texture", self, "thumbnail", G_BINDING_SYNC_CREATE);
}

static void
ee_list_data_class_init (EeListDataClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GParamFlags flags = G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_EXPLICIT_NOTIFY;

	object_class->constructed = ee_list_data_constructed;
	object_class->dispose =  ee_list_data_dispose;
	object_class->finalize = ee_list_data_finalize;
	object_class->set_property = ee_list_data_set_property;
	object_class->get_property = ee_list_data_get_property;

	/* PROPERTIES */

	properties[PROP_IMAGE] = g_param_spec_object ("image", NULL, NULL,
			EE_TYPE_IMAGE,
			flags | G_PARAM_CONSTRUCT_ONLY);

	properties[PROP_THUMBNAIL] = g_param_spec_object ("thumbnail", NULL, NULL,
			GDK_TYPE_TEXTURE,
			flags);

	properties[PROP_SHOW_THUMBNAIL] = g_param_spec_boolean ("show-thumbnail", NULL, NULL,
			TRUE,
			flags | G_PARAM_CONSTRUCT);

	properties[PROP_THUMBNAIL_SIZE] = g_param_spec_enum ("thumbnail-size", NULL, NULL,
			GTK_TYPE_ICON_SIZE,
			GTK_ICON_SIZE_NORMAL,
			flags | G_PARAM_CONSTRUCT);

	g_object_class_install_properties (object_class, N_PROPERTIES, properties);
}

/* PUBLIC METHOD DEFINITIONS */

EeListData *
ee_list_data_new (EeImage *image)
{
	EeListData *data;

	g_return_val_if_fail (EE_IS_IMAGE (image), NULL);

	data = g_object_new (EE_TYPE_LIST_DATA,
			"image", image,
			NULL);

	return data;
}
