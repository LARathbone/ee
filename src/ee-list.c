// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#include "ee-list.h"

#include "ee-util.h"
#include "ee-list-toolbar.h"

#include <glib/gi18n.h>

/* PROPERTIES */

enum
{
	PROP_GEN_THUMBNAILS = 1,
	PROP_USE_LARGE_THUMBNAILS,
	PROP_MODEL,
	PROP_SELECTION,
	N_PROPERTIES
};

static GParamSpec *properties[N_PROPERTIES];

/* GOBJECT DEFINITION */

struct _EeList
{
	GtkWindow parent_instance;

	GListStore *store;
	GtkSelectionModel *selection;

	/* Template */
	GtkWidget *list_view;
	GtkWidget *gen_thumbnails_checkbtn;
	GtkWidget *use_large_thumbnails_checkbtn;
};

G_DEFINE_TYPE (EeList, ee_list, GTK_TYPE_WINDOW)

/* PROPERTIES - GETTERS AND SETTERS */

void
ee_list_set_gen_thumbnails (EeList *self, gboolean gen)
{
	gtk_check_button_set_active (GTK_CHECK_BUTTON(self->gen_thumbnails_checkbtn), gen);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_GEN_THUMBNAILS]);
}

gboolean
ee_list_get_gen_thumbnails (EeList *self)
{
	return gtk_check_button_get_active (GTK_CHECK_BUTTON(self->gen_thumbnails_checkbtn));
}

void
ee_list_set_use_large_thumbnails (EeList *self, gboolean large)
{
	gtk_check_button_set_active (GTK_CHECK_BUTTON(self->use_large_thumbnails_checkbtn), large);
	g_object_notify_by_pspec (G_OBJECT(self), properties[PROP_USE_LARGE_THUMBNAILS]);
}

gboolean
ee_list_get_use_large_thumbnails (EeList *self)
{
	return gtk_check_button_get_active (GTK_CHECK_BUTTON(self->use_large_thumbnails_checkbtn));
}

/* transfer none */
GListModel *
ee_list_get_model (EeList *self)
{
	return G_LIST_MODEL(self->store);
}

/* transfer none */
GtkSelectionModel *
ee_list_get_selection (EeList *self)
{
	return self->selection;
}

static void
ee_list_set_property (GObject *object,
		guint property_id,
		const GValue *value,
		GParamSpec *pspec)
{
	EeList *self = EE_LIST(object);

	switch (property_id)
	{
		case PROP_GEN_THUMBNAILS:
			ee_list_set_gen_thumbnails (self, g_value_get_boolean (value));
			break;

		case PROP_USE_LARGE_THUMBNAILS:
			ee_list_set_use_large_thumbnails (self, g_value_get_boolean (value));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

static void
ee_list_get_property (GObject *object,
		guint property_id,
		GValue *value,
		GParamSpec *pspec)
{
	EeList *self = EE_LIST(object);

	switch (property_id)
	{
		case PROP_GEN_THUMBNAILS:
			g_value_set_boolean (value, ee_list_get_gen_thumbnails (self));
			break;

		case PROP_USE_LARGE_THUMBNAILS:
			g_value_set_boolean (value, ee_list_get_use_large_thumbnails (self));
			break;

		case PROP_MODEL:
			g_value_set_object (value, ee_list_get_model (self));
			break;

		case PROP_SELECTION:
			g_value_set_object (value, ee_list_get_selection (self));
			break;

		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, property_id, pspec);
			break;
	}
}

/* METHOD DEFINITIONS */

/* transfer full */
static char *
basename_closure (EeListData *self, GFile *gfile)
{
	return g_file_get_basename (gfile);
}

static gboolean
use_large_thumbnails_transform_to (GBinding *binding,
		const GValue *from_value,
		GValue *to_value,
		gpointer user_data)
{
	gboolean use_large_thumbnails = g_value_get_boolean (from_value);
	g_value_set_enum (to_value, use_large_thumbnails ? GTK_ICON_SIZE_LARGE : GTK_ICON_SIZE_NORMAL);
	return TRUE;
}

static void
remove_image_action_enable_cb (GListModel *model, guint position, guint removed, guint added, EeList *self)
{
	gtk_widget_action_set_enabled (GTK_WIDGET(self), "win.remove-image",  g_list_model_get_n_items (model) > 0 ? TRUE : FALSE);
}

static void
ee_list_init (EeList *self)
{
	g_type_ensure (EE_TYPE_LIST_DATA);
	g_type_ensure (EE_TYPE_LIST_TOOLBAR);

	gtk_widget_init_template (GTK_WIDGET(self));

	g_object_bind_property (self->gen_thumbnails_checkbtn, "active", self, "generate-thumbnails", G_BINDING_SYNC_CREATE);
	g_object_bind_property (self->use_large_thumbnails_checkbtn, "active", self, "use-large-thumbnails", G_BINDING_SYNC_CREATE);

	gtk_widget_action_set_enabled (GTK_WIDGET(self), "win.remove-image", FALSE);
	g_signal_connect (self->selection, "items-changed", G_CALLBACK(remove_image_action_enable_cb), self);
}

static void
ee_list_dispose (GObject *object)
{
	EeList *self = EE_LIST(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_list_parent_class)->dispose (object);
}

static void
ee_list_finalize (GObject *object)
{
	EeList *self = EE_LIST(object);

	/* Chain up */
	G_OBJECT_CLASS(ee_list_parent_class)->finalize (object);
}

static gboolean
ee_list_close_request (GtkWindow *window)
{
	GObject *app = G_OBJECT(gtk_window_get_application (window));

	gtk_widget_set_visible (GTK_WIDGET(window), FALSE);

	g_object_notify (app, "show-list-window");

	return TRUE;
}

static void
list_remove_accept_or_reject_cb (GtkWindow *dialog, int response_id, gpointer user_data)
{
	EeList *self = EE_LIST(user_data);

	if (response_id == GTK_RESPONSE_ACCEPT)
	{
		EeListData *selected = gtk_single_selection_get_selected_item (GTK_SINGLE_SELECTION(self->selection));

		ee_list_remove_entry (self, selected);
	}

	gtk_window_destroy (GTK_WINDOW(dialog));
}

static void
remove_image_action (EeList *self)
{
	EeListData *selected = gtk_single_selection_get_selected_item (GTK_SINGLE_SELECTION(self->selection));
	EeImage *image = ee_list_data_get_image (selected);

	if (ee_image_get_modified (image))
		ee_accept_or_reject_dialog (GTK_WINDOW(self),
				_("This image has unsaved changes.\n\n"
					"Are you sure you want to remove it from the list?"),
				_("_Remove Anyway"),
				_("_Go Back"),
				list_remove_accept_or_reject_cb,
				self);
	else
		ee_list_remove_entry (self, selected);
}

static void
ee_list_class_init (EeListClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS(klass);
	GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(klass);
	GtkWindowClass *window_class = GTK_WINDOW_CLASS(klass);
	GParamFlags flags = G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS | G_PARAM_CONSTRUCT | G_PARAM_EXPLICIT_NOTIFY;

	object_class->dispose =  ee_list_dispose;
	object_class->finalize = ee_list_finalize;

	object_class->set_property = ee_list_set_property;
	object_class->get_property = ee_list_get_property;

	window_class->close_request = ee_list_close_request;

	/* PROPERTIES */

	properties[PROP_GEN_THUMBNAILS] = g_param_spec_boolean ("generate-thumbnails", NULL, NULL, TRUE, flags);

	properties[PROP_USE_LARGE_THUMBNAILS] = g_param_spec_boolean ("use-large-thumbnails",NULL, NULL, FALSE, flags);

	properties[PROP_MODEL] = g_param_spec_object ("model", NULL, NULL,
			G_TYPE_LIST_MODEL,
			G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

	properties[PROP_SELECTION] = g_param_spec_object ("selection", NULL, NULL,
			GTK_TYPE_SELECTION_MODEL,
			G_PARAM_READABLE | G_PARAM_STATIC_STRINGS);

	g_object_class_install_properties (object_class, N_PROPERTIES, properties);

	/* ACTIONS */

	gtk_widget_class_install_action (widget_class, "win.remove-image", NULL, (GtkWidgetActionActivateFunc) remove_image_action);

	/* TEMPLATE */

	gtk_widget_class_set_template_from_resource (widget_class, "/com/gitlab/LARathbone/EE/ee-list.ui");
	gtk_widget_class_bind_template_child (widget_class, EeList, list_view);
	gtk_widget_class_bind_template_child (widget_class, EeList, store);
	gtk_widget_class_bind_template_child (widget_class, EeList, selection);
	gtk_widget_class_bind_template_child (widget_class, EeList, gen_thumbnails_checkbtn);
	gtk_widget_class_bind_template_child (widget_class, EeList, use_large_thumbnails_checkbtn);

	gtk_widget_class_bind_template_callback (widget_class, basename_closure);
}

/* PUBLIC METHOD DEFINITIONS */

GtkWidget *
ee_list_new (void)
{
	return g_object_new (EE_TYPE_LIST, NULL);
}

void
ee_list_add_entry (EeList *self, EeImage *image /* transfer none */)
{
	EeListData *data;
	guint select_me = 0;

	g_return_if_fail (EE_IS_LIST (self));
	g_return_if_fail (EE_IS_IMAGE (image));

	/* If someone is attempting to add a file that already exists, just select it and return.
	 */
	for (guint i = 0; i < g_list_model_get_n_items (G_LIST_MODEL(self->selection)); ++i)
	{
		EeListData *data_iter = g_list_model_get_item (G_LIST_MODEL(self->selection), i);
		const char *image_path = g_file_peek_path (ee_image_get_file (image));
		const char *data_path = g_file_peek_path (ee_image_get_file (ee_list_data_get_image (data_iter)));

		if (g_strcmp0 (image_path, data_path) == 0)
		{
			select_me = i;
			goto set_selection_and_finish;
		}
	}

	data = ee_list_data_new (image);

	g_object_bind_property (self, "generate-thumbnails", data, "show-thumbnail", G_BINDING_SYNC_CREATE);
	g_object_bind_property_full (self, "use-large-thumbnails", data, "thumbnail-size", G_BINDING_SYNC_CREATE, use_large_thumbnails_transform_to, NULL, NULL, NULL);

	g_list_store_append (self->store, data);

	for (guint i = 0; i < g_list_model_get_n_items (G_LIST_MODEL(self->selection)); ++i)
		if (g_list_model_get_item (G_LIST_MODEL(self->selection), i) == data)
			select_me = i;

set_selection_and_finish:
	gtk_single_selection_set_selected (GTK_SINGLE_SELECTION(self->selection), select_me);
}

void
ee_list_remove_entry (EeList *self, EeListData *data)
{
	gboolean find_retval = FALSE;
	guint pos;

	g_return_if_fail (EE_IS_LIST_DATA (data));

	find_retval = g_list_store_find (self->store, data, &pos);

	if (find_retval)
	{
		g_list_store_remove (self->store, pos);

		/* Removing the item from the underlying list model of the selection
		 * model doesn't necessarily invoke the notify::selected signal, so we
		 * do this manually here. Otherwise, removing entries doesn't
		 * auto-select the previous valid item.
		 */
		g_object_notify (G_OBJECT(self->selection), "selected");
	}
	else
		g_assert_not_reached ();
}
