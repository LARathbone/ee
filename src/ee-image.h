// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#pragma once

#include <gtk/gtk.h>

G_BEGIN_DECLS

/* Enums */

typedef enum {
	EE_IMAGE_CHANNEL_RED,
	EE_IMAGE_CHANNEL_GREEN,
	EE_IMAGE_CHANNEL_BLUE,
	EE_IMAGE_CHANNEL_ALPHA
} EeImageChannel;

typedef enum {
	EE_IMAGE_TEXTURE_TYPE_CLEAN,
	EE_IMAGE_TEXTURE_TYPE_DIRTY
} EeImageTextureType;

typedef enum {
	EE_IMAGE_FLIP_VERTICAL = FALSE,
	EE_IMAGE_FLIP_HORIZONTAL = TRUE
} EeImageFlipType;

/* Boxed */

typedef struct _EeRGBAMod EeRGBAMod;
struct _EeRGBAMod
{
	int red_mod;
	int green_mod;
	int blue_mod;
	int alpha_mod;
};

#define EE_TYPE_RGBA_MOD (ee_rgba_mod_get_type ())

GType ee_rgba_mod_get_type (void) G_GNUC_CONST;
EeRGBAMod * ee_rgba_mod_copy (const EeRGBAMod *rgba_mod);
void ee_rgba_mod_free (EeRGBAMod *rgba_mod);
void ee_rgba_mod_add (const EeRGBAMod *mod1, const EeRGBAMod *mod2, EeRGBAMod *out);

/* Type declaration */

#define EE_TYPE_IMAGE ee_image_get_type()
G_DECLARE_FINAL_TYPE (EeImage, ee_image, EE, IMAGE, GObject)

/* Method declarations */

EeImage *	ee_image_new (GFile *file);
GdkTexture * ee_image_get_dirty_texture (EeImage *self);
GdkTexture * ee_image_get_clean_texture (EeImage *self);
GFile * ee_image_get_file (EeImage *self);
void ee_image_set_file (EeImage *self, GFile *file);
void ee_image_get_rgba_mod (EeImage *self, EeRGBAMod *rgba_mod_out);
void ee_image_apply_changes (EeImage *self);
void ee_image_clear_rgba_mods (EeImage *self);
gboolean ee_image_get_modified (EeImage *self);
int ee_image_get_width (EeImage *self);
int ee_image_get_height (EeImage *self);
void ee_image_set_width (EeImage *self, int width);
void ee_image_set_height (EeImage *self, int height);
void ee_image_restore_original_size (EeImage *self);
GdkPixbufRotation ee_image_get_rotation (EeImage *self);
void ee_image_set_rotation (EeImage *self, GdkPixbufRotation rotation);
void ee_image_flip (EeImage *self, EeImageFlipType flippage);
gboolean ee_image_get_crop_dimensions (EeImage *self, GtkWidget *picture_widget, GtkWidget *parent_widget, GdkRectangle *rect);
void ee_image_crop (EeImage *self, GtkWidget *picture_widget, GtkWidget *parent_widget);
gboolean ee_image_save (EeImage *self, const char *new_filename, GError **error);
GdkPixbuf * ee_image_get_dirty_pixbuf (EeImage *self);

G_END_DECLS
