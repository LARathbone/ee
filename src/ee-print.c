// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#include "ee-print.h"

/* Okay, so this whole source file is pretty lazy/god-awful. I have heard it on
 * good authority that all of this API is likely going away for GTK5 anyway, so
 * we're not going to lose any sleep over it. It works-ish, so let's just be
 * satisfied with that for now.
 */

static void
begin_print (GtkPrintOperation *operation, GtkPrintContext *context, gpointer data)
{
	/* just peg to 1 page for now */
	gtk_print_operation_set_n_pages (operation, 1);
}

static void
draw_page (GtkPrintOperation *operation, GtkPrintContext *context, int page_nr, gpointer data)
{
	EeImage *image = data;
	GdkPixbuf *pixbuf = ee_image_get_dirty_pixbuf (image);
	guchar *pixels = gdk_pixbuf_get_pixels (pixbuf);
	int rowstride = gdk_pixbuf_get_rowstride (pixbuf);
	int width = gdk_pixbuf_get_width (pixbuf);
	int height = gdk_pixbuf_get_height (pixbuf);
	cairo_t *cr = gtk_print_context_get_cairo_context (context);
	cairo_surface_t *surface = cairo_image_surface_create_for_data (pixels, CAIRO_FORMAT_ARGB32, width, height, rowstride);

	cairo_set_source_surface (cr, surface, 0, 0);
	cairo_paint (cr);

	cairo_surface_destroy (surface);
}

void
ee_print (EeImage *image)
{
	g_autoptr(GError) local_error = NULL;
	g_autoptr(GtkPrintOperation) print = NULL;
	GtkPrintOperationResult res;

	g_return_if_fail (EE_IS_IMAGE (image));

	print = gtk_print_operation_new ();

	g_signal_connect (print, "begin-print", G_CALLBACK(begin_print), NULL);
	g_signal_connect (print, "draw-page", G_CALLBACK(draw_page), image);

	res = gtk_print_operation_run (print,
			GTK_PRINT_OPERATION_ACTION_PRINT_DIALOG,
			GTK_WINDOW(ee_get_application_window ()),
			&local_error);

	if (res == GTK_PRINT_OPERATION_RESULT_ERROR)
		ee_error_dialog (NULL, local_error);
}
