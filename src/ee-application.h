// vim: ts=4 sw=4 breakindent breakindentopt=shift\:4 wrap linebreak mouse=a

#pragma once

#include <gtk/gtk.h>

#include "ee-application-window.h"

G_BEGIN_DECLS

/* Type declaration */

#define EE_TYPE_APPLICATION ee_application_get_type()
G_DECLARE_FINAL_TYPE (EeApplication, ee_application, EE, APPLICATION, GtkApplication)

/* Method declarations */

EeApplication *	ee_application_new (void);
void ee_application_open_file (EeApplication *self, GFile *file);
EeApplicationWindow * ee_application_get_application_window (EeApplication *self);
GSettings * ee_application_get_settings (EeApplication *self);
void ee_application_quit (EeApplication *self);

G_END_DECLS
